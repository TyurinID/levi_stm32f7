
#ifndef USD_API_H
#define USD_API_H

#include "main.h"
#include "ff.h"
#include "ff_gen_drv.h"
#include "cmsis_os.h"
#include "sd_diskio.h"
#include "stm32746g_discovery_ts.h"
#include "cmsis_os.h"

#define INJECTORS_NUM	1024					// max number of injectors
#define INJECTOR_NAME_LENGTH 10			

typedef struct
{
	uint32_t	InjectorCode;
	uint32_t	ImageType;
	float			ParamNominal[5];
	float			ParamTolerance[5];
	uint32_t	Current;
	uint32_t	Time;
} TInjector;						

extern TInjector	GInjector;	// Element of Inj Database.

typedef enum {
	GS_OK = 0,				// (0) Geting string successful.
	GS_EOF,						// (1) The End Of File detected. 
	GS_ERROR					// (2) Error.
} GSRESULT;				// results of function SD_GetString().

extern uint16_t InjectorMaxNum;			// max number of injectors

extern FIL MyFile;     /* File object */
extern FATFS SDFatFs;  /* File system object for SD card logical drive */
extern char SDPath[4]; /* SD card logical drive path */

extern FRESULT SD_Error;					// look for FRESULT structure in ff.h

extern void SD_InitFS(void);							// Function initialize the SD Card driver. Check the appearence of SD card,
																					//  register the file system object to the FatFs module.

extern void	SD_OpenFile(const TCHAR* path, BYTE mode);			//  Open file (path) for writing or reading (mode). In case of an error the function returns
																												//  and an error number will be saved in variable "SD_Error". 

extern GSRESULT SD_GetString(TCHAR* string, int size);	// Function read the string from opend file to (string) with (size). (string) should be big enougth for string! Returns "0".
																											// If any error occure fuction returns "2" and type of Error is saved in "SD_Error". 
																											// If EOF function returns "1", but "SD_Error" is clear.

extern int SD_WriteString(TCHAR* string);				// Function write (string) to opend file. Returns numer of written symbols.

extern void SD_CloseFile(void);				// Function closes the last opend file. 

extern void LoadBase(void);						// Load Inj Database from uSD.

#endif
