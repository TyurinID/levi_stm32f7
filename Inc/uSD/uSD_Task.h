
#ifndef USD_TASK_H
#define USD_TASK_H

#include "cmsis_os.h"
#include "sd_diskio.h"
#include "uSD/uSD_API.h"

#define INJECTORS_NUM	512
#define INJECTOR_NAME_LENGTH 10
		
typedef struct
{
	uint32_t	InjectorCode;
	uint32_t	ImageType;
	float			ParamNominal[5];
	float			ParamTolerance[5];
} TInjector;						// Element of Base.

extern TInjector	GInjector;	

//typedef struct
//{
//	uint32_t 	InjectorCode;
//	
//	DWORD 		BasePosition;
//	uint16_t	BaseLineNumber;
//	uint8_t	 	ParametersNumber;
//} T_uSD_BufferRegister;

extern uint16_t InjectorMaxNum;

extern void uSD_Thread(void const *argument);    // uSD task for main application

extern void readBase(void);						// Pick out injectors_codes from base.

extern void Error_Handler(void);

extern uint8_t	GWindowNumber;

#endif