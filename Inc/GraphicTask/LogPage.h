/**
  ************************************************************************************************
  * @file	   LogPage.h
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Header for LogPage.c.
	*					Set here coordinates of all labels and fields.
  *************************************************************************************************
  */
	
#ifndef LOGDLG
#define LOGDLG

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"

// -------------------------------------------------------- Exported define ----------------------:

#define LOG_LINE_LENGTH				50
#define LOG_LINES_BUFF_SIZE		30

// Log strings(lines) are displayed in table ===========:
#define LOG_LINES_NUM  			24

#define LOG_TABLE1_X0				20
#define LOG_TABLE1_X1				230
#define LOG_TABLE2_X0				240
#define LOG_TABLE2_X1				460

#define LOG_TABLE_Y0				65
#define LOG_TABLE_Y1				260

// -------------------------------------------------------- Exported types -----------------------:
typedef struct
{
	uint16_t	currentLine;
	char			line[LOG_LINES_BUFF_SIZE][LOG_LINE_LENGTH];
}	tLog;

typedef	struct
{
	uint8_t	  Active;
}	tLogWin;

// -------------------------------------------------------- Exported variables -------------------: 
//extern tLog		gLog; 
extern	tLogWin		gLogWin;

// -------------------------------------------------------- Exported functions -------------------: 
extern void AddLog(char* text);	// Add log string in log table for displaing on LogPage.

extern void LogPage_Init(void);
extern void LogPage_Draw(void);
extern void LogPage_Process(__IO TS_StateTypeDef  ts);

#endif


