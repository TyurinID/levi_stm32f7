
#ifndef GRAPHICTASK_H
#define GRAPHICTASK_H

#include "cmsis_os.h"

extern uint8_t GToDrawFLG;

extern void GraphicThread(void const *argument);    // graphic task for main application

extern void DrawWAIT(void);

// colors:								from light to dark
#define GUI_BLUE_00       0x00E6E0B0
#define GUI_BLUE_01       0x00E6C0A0
#define GUI_BLUE_02       0x00ED9564
#define GUI_BLUE_03       0x00E16941

// colors:								from light to dark
#define GUI_TEAL_00       0x00E6E0B0
#define GUI_TEAL_01       0x00D6CF9B
#define GUI_TEAL_02       0x00C19579
#define GUI_TEAL_03       0x00B3AB67
#define GUI_TEAL_04       0x009B934F
#define GUI_TEAL_05       0x008C843F
#define GUI_TEAL_06       0x007D5732
#define GUI_TEAL_07       0x00696223
#define GUI_TEAL_08       0x00544E17

#endif

