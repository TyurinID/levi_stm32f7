
#ifndef GRAPH_KEYBOARD_H
#define GRAPH_KEYBOARD_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/GraphicCommon.h"

#include <math.h>
#include <string.h>

#define KEY_BOARD_DIGITS 11

typedef struct
{
	uint32_t 	*Target;
	uint8_t 	digits[KEY_BOARD_DIGITS];
	uint8_t 	digits_length;
	
	uint16_t	Coord[4];		// x0, x1, y0, y1 .
	uint8_t 	Active;  // active
	uint32_t 	FMin;		// min value
	uint32_t 	FMax;  	// max value
	
} TKeyBoard;

extern TKeyBoard GKeyBoard;

extern void KeyBoard_Init(void);			// TI: keyboard characteristics on the screen

extern void KeyBoard_Draw(void);			// TI: 

extern void KeyBoard_Process(__IO TS_StateTypeDef  ts);		// TI: 

#endif
