
#ifndef GRAPH_ERROR
#define GRAPH_ERROR

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/GraphicCommon.h"
#include "GraphicTask/GraphKeyBoard.h"
#include "uSD/uSD_API.h"

#include <string.h>
#include <math.h>

typedef struct
{
	uint8_t		Active;
	char			Text[64];
	uint8_t		Code;
	} TErrorWin;
extern TErrorWin GErrorWin;	

extern void ErrorWin_Init(void);
extern void ErrorWin_Process(__IO TS_StateTypeDef ts);				// Action after touching the screen.
extern void ErrorWin_Draw(void);									// Create error window with text of error.

#endif
