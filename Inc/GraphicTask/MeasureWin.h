
#ifndef MEASURE_WINDOW
#define MEASURE_WINDOW

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/GraphicCommon.h"
#include "GraphicTask/GraphKeyBoard.h"
#include "GraphicTask/GraphWarning.h"
#include "uSD/uSD_API.h"

#include <string.h>
#include <math.h>

// Display Fields positions:
#define		FIELDS_X0			15
#define		FIELDS_X1			165

#define		LABEL1_Y0			70
#define		LABEL1_Y1			85
#define		FIELD1_Y0			85
#define		FIELD1_Y1			120

#define		LABEL2_Y0			125
#define		LABEL2_Y1			140
#define		FIELD2_Y0			140
#define		FIELD2_Y1			170

#define		KNOB_Y0				200
#define		KNOB_Y1				250

//Display Table positions:
#define		COLUMN1_X0		195
#define		COLUMN2_X0		275
#define		COLUMN3_X0		365
#define		COLUMN3_X1		465

#define		LINE1_Y0			70
#define		LINE2_Y0			100
#define		LINE3_Y0			130
#define		LINE4_Y0			160
#define		LINE5_Y0			190
#define		LINE6_Y0			220
#define		LINE6_Y1			250
#define		SUBSCRIPTION_Y1		265


//#define	COOLING_DELAY_COEF		2			

typedef	struct
{
	uint8_t	  Active;
}	TMeasureWin;
extern	TMeasureWin		GMeasureWin;

extern TInjector 	GSelectedInjector;
extern uint32_t		GSelectedInj;
extern uint8_t		GMeasurementStart;
extern uint8_t		GInjChanged;
extern uint8_t		GParamChanged;
extern uint32_t 	GCurrent;
extern uint32_t 	GTime;	

extern void MeasureWin_Init(void);						// Create buttons and inscriptions first time. Initialize structure.
extern void MeasureWin_Draw(void);						// Create buttons and inscriptions.
extern void MeasureWin_Process(__IO TS_StateTypeDef  ts);

extern void DrawWAIT(void);

#endif
