
#ifndef SCREEN_SECTORS_H
#define SCREEN_SECTORS_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include "cmsis_os.h"

typedef struct
{
	int FXBegin;
	int FXEnd;
	int FYBegin;
	int FYEnd;
} TScreenSector;

#define SCREEN_SECTORS_HORIZONTAL_CNT 3

#define X_SECTORS_OVERLAP 3

extern TScreenSector GScreen_sector[2*SCREEN_SECTORS_HORIZONTAL_CNT];

extern void InitializeScreenSectors(void);

/*
extern void DrawLineInScreenSector(int x1, int y1, int x2, int y2, TScreenSector* screen_sector);

extern void DrawRectInScreenSector(int x1, int y1, int x2, int y2, TScreenSector* screen_sector);

extern void FillRectInScreenSector(int x1, int y1, int x2, int y2, TScreenSector* screen_sector);

extern void DrawEllipseInScreenSector(int x0, int y0, int rad_x, int rad_y, TScreenSector* screen_sector);

extern void FillEllipseInScreenSector(int x0, int y0, int rad_x, int rad_y, TScreenSector* screen_sector);

extern void DrawTextInScreenSector(const char *s, int x1, int y1, TScreenSector* screen_sector);
*/

#endif

