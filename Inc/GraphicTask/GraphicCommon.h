

#ifndef GRAPH_COMMON_H
#define GRAPH_COMMON_H

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"

#include <math.h>
#include <string.h>



extern GUI_MEMDEV_Handle hMem;

extern uint8_t blink_cnr;


extern uint8_t GGraphFuncNum; // global graph screen Id



#endif
