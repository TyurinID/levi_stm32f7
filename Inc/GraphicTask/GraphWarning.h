
#ifndef GRAPH_WARNING
#define GRAPH_WARNING

#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"
#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/GraphicCommon.h"
#include "GraphicTask/GraphKeyBoard.h"
#include "uSD/uSD_API.h"

#include <string.h>
#include <math.h>

typedef	struct
{
	uint8_t	  Active;
	char	  	Text[128];
}	TWarningWin;
extern	TWarningWin		GWarningWin;


extern void WarningWin_Init(void);
extern void WarningWin_Draw(void);
extern void WarningWin_Process(__IO TS_StateTypeDef  ts);				// Warning message, if user click current or time label.
extern void WarningWin_Process2(__IO TS_StateTypeDef  ts);
extern void WarningWin_Process3(__IO TS_StateTypeDef  ts);				// Warning message: "Injector Code not Found".

#endif
