
#ifndef APPLICATION_COMMON_H
#define APPLICATION_COMMON_H

#include "main.h"
#include "cmsis_os.h"
#include "TagSystem/TagSystem.h"


typedef struct
{
	uint32_t byte1;
	uint32_t byte2;
	uint32_t byte3;
	uint32_t byte4;
} TIpAddr;

extern TIpAddr GIpAddress;
extern TIpAddr GNetMask;
extern TIpAddr GDefaultGateway;


#endif

