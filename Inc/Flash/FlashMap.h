
#ifndef FLASH_MAP_H
#define FLASH_MAP_H

#include "main.h"
#include "cmsis_os.h"
#include "uSD/uSD_API.h"

//#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base address of Sector 0, 32 Kbytes */
//#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08008000) /* Base address of Sector 1, 32 Kbytes */
//#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08010000) /* Base address of Sector 2, 32 Kbytes */
//#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x08018000) /* Base address of Sector 3, 32 Kbytes */
//#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08020000) /* Base address of Sector 4, 128 Kbytes */
//#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08040000) /* Base address of Sector 5, 256 Kbytes */
//#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08070000) /* Base address of Sector 6, 256 Kbytes */
//#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x080A0000) /* Base address of Sector 7, 256 Kbytes */

#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base address of Sector 0, 32 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08008000) /* Base address of Sector 1, 32 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08010000) /* Base address of Sector 2, 32 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x08018000) /* Base address of Sector 3, 32 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08020000) /* Base address of Sector 4, 128 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08040000) /* Base address of Sector 5, 256 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x080C0000) /* Base address of Sector 6, 256 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08100000) /* Base address of Sector 7, 256 Kbytes */


#define FLASH_USER_START_ADDR     0x080C0000 /* Base address of Sector 6, 256 Kbytes */

//#define FLASH_USER_START_ADDR     0x08040000 /* Base address of Sector 5, 256 Kbytes */
#define FLASH_USER_END_ADDR     ADDR_FLASH_SECTOR_7 /* Base address of Sector 6, 256 Kbytes */

		//====================== TI:

		
//		typedef struct
//		{
//			uint32_t *FTarget;
//			uint32_t FTargetCopy;
//		} TFlashMapRegister;

//		extern TFlashMapRegister GFlashMapRegister[NUM_OF_INJECTORS]; // Contain pointers and copies of Injector_Codes in Flash
//		
//		extern void FlashInit(void);			// Create in Flash pseudo database for experiment
		
		//====================== TI.



#define FLASHMAP_COUNT 1024


typedef struct
{
	uint32_t *FTarget;
	uint32_t FBufferValue;
} TFlashSavingRegister;

extern void WriteFlash(uint16_t InjNumber);				// TI: write in Flash data about injector. 

extern void EraseFlash(void);				// TI: erase Flash data about injectors. 

extern TInjector ReadFlash(uint32_t InjCode);

extern TFlashSavingRegister GFlashSavingRegister[FLASHMAP_COUNT]; // flash data map

extern uint16_t FlasMapInit(void);								// 


#endif

