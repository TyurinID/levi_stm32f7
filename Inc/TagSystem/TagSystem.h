
#ifndef TAGSYSTEM_H
#define TAGSYSTEM_H

#include "main.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>


// Output holding registers
typedef struct 
{
	uint16_t *FValue;  //pointer to tag value
	
	#ifdef USE_RETAINED_TAGS
	bool FRetained;   // save tag value during power down
	#endif
	
	uint16_t FEepromAdress; // adres in eeprom if register is retained
} TUint16Tag;   // uint16 tag structure

#define MODBUS_OUTPUT_HOLDING_REGISTERS_COUNT 20
extern TUint16Tag ModbusOutputHoldingRegisters[MODBUS_OUTPUT_HOLDING_REGISTERS_COUNT];

extern TUint16Tag* CreateUint16Tag(uint16_t *TagSource, uint16_t Index);  // create non-retained uint16 tag

#ifdef USE_RETAINED_TAGS
extern TUint16Tag* CreateUint16RetainedTag(uint16_t *TagSource, uint16_t EepromAddress, uint16_t Index); // create retained uint16_t tag
#endif

extern uint16_t GetUint16TagValue(TUint16Tag tag);  // get value of the tag 
extern void SetUint16TagValue(TUint16Tag tag, uint16_t value);  // set tag value

// input registers
#define MODBUS_INPUT_REGISTERS_COUNT 150
extern TUint16Tag ModbusInputRegisters[MODBUS_INPUT_REGISTERS_COUNT];

extern TUint16Tag* CreateUint16InputTag(uint16_t *TagSource, uint16_t Index);  // create input uint16 tag

// Coil tag definitions
typedef struct
{
	bool *FValue;  // pointer to tag value
} TBoolTag;    // bool tag register

#define MODBUS_DISCRETE_COILS_COUNT 20
extern TBoolTag ModbusDiscreteCoils[MODBUS_DISCRETE_COILS_COUNT];

extern TBoolTag *CreateBoolTag(bool *TagSource, uint16_t Index);
extern uint8_t GetBoolTagValueBitmask(TBoolTag *table, uint16_t StartAdress, uint8_t Count);
extern void SetBoolValueBitmask(TBoolTag *table, uint8_t Bitmask, uint8_t Count);

// input coils
#define MODBUS_DISCRETE_INPUTS_COUNT 20
extern TBoolTag ModbusDiscreteInputs[MODBUS_DISCRETE_INPUTS_COUNT];

extern TBoolTag *CreateBoolInputTag(bool *TagSource, uint16_t Index);

extern void MakeTagsPadding(void);

#endif
