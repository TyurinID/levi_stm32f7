#ifndef UART_H
#define UART_H


#include "main.h"
#include "cmsis_os.h"
#include "GraphicTask/LogPage.h"

/////////////////////
// UART
/////////////////////


// -------------------------------------------------------- Private macros -----------------------:
#define RELOAD_UART(uart)											\
		if(HAL_UART_DeInit(uart) != HAL_OK)			\
			__NOP();																\
		if(HAL_UART_Init(uart) != HAL_OK)				\
			__NOP();																\
		osDelay(100);






// uart

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* User can use this section to tailor USARTx/UARTx instance used and associated 
   resources */
/* Definition for USARTx clock resources */
#define USARTx                           USART6
#define USARTx_CLK_ENABLE()              __USART6_CLK_ENABLE()
#define DMAx_CLK_ENABLE()                __HAL_RCC_DMA2_CLK_ENABLE()
#define USARTx_RX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()

#define USARTx_FORCE_RESET()             __USART6_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __USART6_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_6
#define USARTx_TX_GPIO_PORT              GPIOC
#define USARTx_TX_AF                     GPIO_AF8_USART6
#define USARTx_RX_PIN                    GPIO_PIN_7
#define USARTx_RX_GPIO_PORT              GPIOC
#define USARTx_RX_AF                     GPIO_AF8_USART6

/* Size of Trasmission buffer */
#define TXBUFFERSIZE                      (COUNTOF(aTxBuffer))					// todo: 
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))
/* Size of Reception buffer */
#define RXBUFFERSIZE                      TXBUFFERSIZE - 1				// minus address byte

/* Definition for USARTx's DMA */
#define USARTx_TX_DMA_STREAM              DMA2_Stream6
#define USARTx_RX_DMA_STREAM              DMA2_Stream1
#define USARTx_TX_DMA_CHANNEL             DMA_CHANNEL_5
#define USARTx_RX_DMA_CHANNEL             DMA_CHANNEL_5

/* Definition for USARTx's NVIC */
#define USARTx_DMA_TX_IRQn                DMA2_Stream6_IRQn
#define USARTx_DMA_RX_IRQn                DMA2_Stream1_IRQn
#define USARTx_DMA_TX_IRQHandler          DMA2_Stream6_IRQHandler
#define USARTx_DMA_RX_IRQHandler          DMA2_Stream1_IRQHandler

/* Definition for USARTx's NVIC */
#define USARTx_IRQn                      USART6_IRQn
#define USARTx_IRQHandler                USART6_IRQHandler


//protocol
// type of the message
typedef enum
{
	StartSingleImpulse = 1,			// 1: start single injector impulse
	
	SetHold1Current=2,    // 2: set hold1 phase current
	SetHold1Time=3,       // 3: set hold1 phase time (in us)
	
	ReadHold1Current=4,    			// 4: set hold1 phase current
	ReadHold1Time=5,       			// 5: set hold1 phase time (in us)
	
	Debug=6,              // debug
	
	NoMessageDetected          // no message detected
} TMessageType;

extern TMessageType GUartMessageType; 

typedef enum
{
	UART_OK 	= 0,		// No errors. 
	TX_ERR	 	= 1,		// 
	RX_ERR		= 2,   	// 
	VERIFY_ERR= 3,    // 3: set hold1 phase time (in us)
	TIMEOUT_ERR=4,		// Timeout = 255 ms;
} TUART_Error;

typedef struct
{
	TMessageType 	FType; 						// type of the message
	uint32_t 			FParameter; 			// pointer to the parameter
}	TProtocol;

extern TUART_Error	GUART_Error;
extern uint8_t UartInit(void);
extern void UART_Thread(void const *argument);
extern uint8_t GDelay_ON;


/////////////////////
// UART
/////////////////////



#endif

