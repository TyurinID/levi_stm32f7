
#include "uSD/uSD_API.h"
#include "FLash/FlashMap.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>
      
static uint8_t CheckBase(void);
static void	DecodeLine(char* str, TInjector* injector);
			
TInjector	GInjector;
uint16_t InjectorMaxNum;

int number;

FATFS SDFatFs;  /* File system object for SD card logical drive */
FIL MyFile;     /* File object */
char SDPath[4]; /* SD card logical drive path */
FRESULT SD_Error = FR_OK;

void SD_InitFS(void)
{
	if(FATFS_LinkDriver(&SD_Driver, SDPath) == 0)				// Link the micro SD disk I/O driver.
	{
		if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)		// Register the file system object to the FatFs module.
		{
			SD_Error = FR_INVALID_PARAMETER;
		}
	}
}

void	SD_OpenFile(const TCHAR* path, BYTE mode)
{
	SD_Error = f_open(&MyFile, path, FA_READ);     	// Open the text file object with read access.
}
 
GSRESULT SD_GetString(TCHAR* string, int size)
{
	f_gets(string, size, &MyFile);
	if((SD_Error = (FRESULT)f_error(&MyFile)) != FR_OK)	
	{
		return GS_ERROR;
	}
	if(f_eof(&MyFile))	return GS_EOF;
	return GS_OK;
}

int SD_WriteString(TCHAR* string)
{
	FRESULT res = FR_OK;
	res = f_lseek(&MyFile, f_size(&MyFile));
	if( res )			// If Error:
	{
		SD_Error = res;	
		return (-1);
	}
	return (f_puts(string, &MyFile));
}

void SD_CloseFile()
{
	FRESULT res = FR_OK;
	
	res = f_close(&MyFile);														// Close the file.
	if( res )	// If Error:
	{
		SD_Error = res;	
		return;
	}
}

static void readBase(void)
{
		uint16_t lines_counter = 0;
	  uint16_t inj_counter = 0xffff; // -1
		char str[128] = "";
		uint32_t	inj_new = 0;
		uint32_t	inj_old = 0;
	
		f_lseek(&MyFile, 0);
	
		while((FRESULT)SD_GetString(str, 128) == FR_OK)								// Read and analize line by line.
		{			
			if(lines_counter != 0)				// Skip the first line.
			{
				inj_new = atoi(str);							// Pick out injector code.	
				if (inj_new != inj_old)		// If inj_new differs from inj_old, create a new injector_object in uSD_BufferRegister.
				{
					if(inj_counter != 0xFFFF)
						WriteFlash(inj_counter);
										
					inj_counter++;						
					
					inj_old = inj_new;
					GInjector.InjectorCode = inj_new;				// Injector code.
					for(uint8_t p=0; p<5; p++)							// Reset parameters.
					{
					GInjector.ParamNominal[p] = 0.0f;
					GInjector.ParamTolerance[p] = 0.0f;
					}
					GInjector.Current	= 3;
					GInjector.Time = 100;
				}
				DecodeLine(str, &GInjector);
			}
			lines_counter++;
		}
		
		// Decode last line:
		DecodeLine(str, &GInjector);
				
		if(inj_counter != 0xFFFF)					// Write last Injector to the Flash.
						WriteFlash(inj_counter);
		InjectorMaxNum = inj_counter+1;			
}

void LoadBase(void)						// Load InjBase from uSD.
{
	InjectorMaxNum = INJECTORS_NUM;			// todo: save in Flash.
	
	SD_InitFS();					
	
	SD_OpenFile("base.csv", FA_READ);

	if(CheckBase() == 1)				// If there is any diffrences.
	{	
		EraseFlash();		
		readBase();
	}
	
	SD_CloseFile();		
}	


uint8_t	CheckBase(void)
{
	uint8_t res = 0;
	
	InjectorMaxNum = INJECTORS_NUM;			// todo: save in Flash.
		
	uint16_t lines_counter = 0;
	uint16_t inj_counter = 0xffff; // -1
	char str[128] = "";
	uint32_t	inj_new = 0;
	uint32_t	inj_old = 0;

	f_lseek(&MyFile, 0);

	while((FRESULT)SD_GetString(str, 128) == FR_OK)								// Read and analize line by line.
	{			
		if(lines_counter != 0)				// Skip the first line.
		{
			inj_new = atoi(str);							// Pick out injector code.	
			if (inj_new != inj_old)		// If inj_new differs from inj_old, create a new injector_object in uSD_BufferRegister.
			{		
				inj_counter++;
				inj_old = inj_new;
				uint32_t addr = FLASH_USER_START_ADDR + inj_counter*sizeof(TInjector);
				uint32_t temp = *((__IO uint32_t *)(addr));
				if(inj_new != temp)
				{
					res = 1;
					break;
				}
			}
		}
		lines_counter++;
	}
	
	if(res == 0)
		InjectorMaxNum = inj_counter+1;		
	return res;
}

void	DecodeLine(char* str, TInjector* injector)
{	
	// Decode "injector_img_type" ==== :
	uint8_t k = 11;	
	uint8_t j = 0;
	char str1[4] = "";
	char  param_tmp[4]	= "";
	float	nominal_tmp = 0.0f;
	float toleran_tmp = 0.0f;
	
	
	while(str[k + j] != ',')	
	{
		str1[j] = str[k + j];
		j++;
	}
	str1[j++] = '\0';
	k += j;
			
	if(strcmp(str1, "CRI1") == 0)
		injector->ImageType = 0;
	else	if(strcmp(str1, "CRI2.0") == 0)
		injector->ImageType = 1;
	else	if(strcmp(str1, "CRI2.1") == 0)
		injector->ImageType = 2;
	else	if(strcmp(str1, "CRI2.2") == 0)
		injector->ImageType = 3;
	else	if(strcmp(str1, "CRIN1") == 0)
		injector->ImageType = 4;
	else	if(strcmp(str1, "CRIN2-A") == 0)
		injector->ImageType = 5;
	else	if(strcmp(str1, "CRIN3") == 0)
		injector->ImageType = 6;
	else	if(strcmp(str1, "CRIN3-L") == 0)
		injector->ImageType = 7;
	else	
		injector->ImageType = 8;

// Decode parameter ==========:
	j = 0;
	while(str[k + j] != ',')	
	{
		param_tmp[j] = str[k + j];
		j++;
	}		
	param_tmp[j++] = '\0';
	k += j;
					
	j = 0;
	// "nominal" reading:
	while(str[k + j] != ',')	
	{
		str1[j] = str[k + j];
		j++;
	}	
	str1[j++] = '\0';
	k += j;
	nominal_tmp = atof(str1);			// Pick out parameter nominal.
				
	j = 0;
	// "tolerance" reading:
	while(str[k + j] != ',')	
	{
		str1[j] = str[k + j];
		j++;
	}	
	str1[j++] = '\0';
	k += j;
	toleran_tmp = atof(str1);			// Pick out parameter tolerance.

	// Select the parameter ==========:
	uint8_t param_num = 0;
	if(strcmp(param_tmp, "AH") == 0)
		param_num = 0;
	else	if(strcmp(param_tmp, "UEH") == 0)
		param_num = 1;
	else	if(strcmp(param_tmp, "RLS") == 0)
		param_num = 2;
	else	if(strcmp(param_tmp, "AHE") == 0)
		param_num = 3;
	else	
		param_num = 4;
		
	injector->ParamNominal[param_num] = nominal_tmp;
	injector->ParamTolerance[param_num] = toleran_tmp;
	
	j = 0;
	// "Current" reading:
	while(str[k + j] != ',')	
	{
		str1[j] = str[k + j];
		j++;
	}
	str1[j++] = '\0';
	k += j;
	injector->Current = atoi(str1);			// Pick out parameter Current.
	
	j = 0;
	// "Time" reading:
	while(str[k + j] != '\n')	
	{
		str1[j] = str[k + j];
		j++;
	}
	str1[j++] = '\0';
	k += j;
	injector->Time = atoi(str1);			// Pick out parameter Time.
}
