
#include "main.h"
#include "uSD/uSD_Task.h"
#include "uSD/uSD_API.h"
#include "Flash/FlashMap.h"
#include <string.h>
#include <stdlib.h>

//T_uSD_BufferRegister G_uSD_BufferRegister[INJECTORS_NUM];      
TInjector	GInjector;

uint32_t Test1 = sizeof(TInjector);

uint8_t RepeatSDRead_FLG;
uint8_t	GWindowNumber;
uint16_t InjectorMaxNum;
int number;

void uSD_Thread(void const *argument)
{	
	GWindowNumber = 0;
	InjectorMaxNum = 0;
	osDelay(1000);
	
	EraseFlash();
	
	// Read base:
		SD_OpenFile("base.csv", FA_READ);
		while(SD_Error)				// If any error occured, user should read a message on display and push "REPEAT" befour continuing.
		{
			HAL_Delay(10);
		}					
				
		readBase();
		while(SD_Error)				// If any error occured, user should read a message on display and push "REPEAT" befour continuing.
		{
			HAL_Delay(10);
		}	

		SD_CloseFile();
		while(SD_Error)				// If any error occured, user should read a message on display and push "REPEAT" befour continuing.
		{
			HAL_Delay(10);
		}					


	GWindowNumber = 1;
	
  // Infinite Loop 
  for( ;; )
  {
  }
}


void readBase(void)
{
		uint16_t lines_counter = 0;
	  uint16_t inj_counter = 0xffff; // -1
		char str[128] = "";
		char str1[8] = "";
		uint32_t	inj_new = 0;
		uint32_t	inj_old = 0;
		uint8_t k = 0;
		uint8_t j = 0;
	
		char  param_tmp[4]	= "";
		float	nominal_tmp = 0.0f;
		float toleran_tmp = 0.0f;
	
		f_lseek(&MyFile, 0);
	
		while((FRESULT)SD_GetString(str, 128) == FR_OK)								// Read and analize line by line.
		{			
			if(lines_counter != 0)				// Skip the first line.
			{
				inj_new = atoi(str);							// Pick out injector code.	
				if (inj_new != inj_old)		// If inj_new differs from inj_old, create a new injector_object in uSD_BufferRegister.
				{
					if(inj_counter != 0xFFFF)
						WriteFlash(inj_counter);
										
					inj_counter++;						
					
					inj_old = inj_new;
					GInjector.InjectorCode = inj_new;				// Injector code.
					for(uint8_t p=0; p<5; p++)							// Reset parameters.
					{
					GInjector.ParamNominal[p] = 0.0f;
					GInjector.ParamTolerance[p] = 0.0f;
					}
				}
				
				// Decode "injector_img_type" ==== :
				k = 11;	
				j = 0;
				while(str[k + j] != ',')	
				{
					str1[j] = str[k + j];
					j++;
				}
				str1[j++] = '\0';
				k += j;
						
				if(strcmp(str1, "CRI1") == 0)
					GInjector.ImageType = 0;
				else	if(strcmp(str1, "CRI2.0") == 0)
					GInjector.ImageType = 1;
				else	if(strcmp(str1, "CRI2.1") == 0)
					GInjector.ImageType = 2;
				else	if(strcmp(str1, "CRI2.2") == 0)
					GInjector.ImageType = 3;
				else	if(strcmp(str1, "CRIN1") == 0)
					GInjector.ImageType = 4;
				else	if(strcmp(str1, "CRIN2.A") == 0)
					GInjector.ImageType = 5;
				else	if(strcmp(str1, "CRIN3") == 0)
					GInjector.ImageType = 6;
				else	if(strcmp(str1, "CRIN3") == 0)
					GInjector.ImageType = 7;
			
			// Decode parameter ==========:
				j = 0;
				while(str[k + j] != ',')	
				{
					param_tmp[j] = str[k + j];
					j++;
				}		
				param_tmp[j++] = '\0';
				k += j;
								
				j = 0;
				// "nominal" reading:
				while(str[k + j] != ',')	
				{
					str1[j] = str[k + j];
					j++;
				}	
				str1[j++] = '\0';
				k += j;
				nominal_tmp = atof(str1);			// Pick out parameter nominal.
							
				j = 0;
				// "tolerance" reading:
				while(str[k + j] != '\n')	
				{
					str1[j] = str[k + j];
					j++;
				}	
				str1[j++] = '\0';
				k += j;
				toleran_tmp = atof(str1);			// Pick out parameter tolerance.
			
				// Select the parameter ==========:
				uint8_t param_num = 0;
				if(strcmp(param_tmp, "AH") == 0)
					param_num = 0;
				else	if(strcmp(param_tmp, "UEH") == 0)
					param_num = 1;
				else	if(strcmp(param_tmp, "RLS") == 0)
					param_num = 2;
				else	if(strcmp(param_tmp, "AHE") == 0)
					param_num = 3;
				else	
					param_num = 4;
					
				GInjector.ParamNominal[param_num] = nominal_tmp;
				GInjector.ParamTolerance[param_num] = toleran_tmp;
			}
			lines_counter++;
		}
		if(inj_counter != 0xFFFF)					// Write last Injector to the Flash.
						WriteFlash(inj_counter);
		InjectorMaxNum = inj_counter+1;		
}



void Error_Handler(void)  // This function is executed in case of error occurrence.
{
  /* Turn LED1 on */
  BSP_LED_On(LED1);
	
//  while(1)
//  {
//    BSP_LED_Toggle(LED1);
    HAL_Delay(500);
	BSP_LED_Off(LED1);
//  }
}
