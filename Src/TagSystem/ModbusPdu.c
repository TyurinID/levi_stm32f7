
#include "TagSystem/ModbusPdu.h"


static void ReadHoldingRegisters(uint16_t registers_offset, uint16_t registers_number,
																 uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	uint16_t temp;
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_OUTPUT_HOLDING_REGISTERS_COUNT >= registers_offset + registers_number)
	{
			transmit_buf[0] = 0x03;
			transmit_buf[1] = ((uint8_t)registers_number)*2;
			for(uint16_t i=0; i<registers_number; i++)
			{
				temp = GetUint16TagValue(ModbusOutputHoldingRegisters[registers_offset+i]);
				transmit_buf[2+2*i] = (uint8_t)(temp>>8);
				transmit_buf[2+2*i+1] = (uint8_t)(temp & 0x00ff);
			}
						
			*(tr_buf_size) = 2 + 2*registers_number;
	}
	else
	{
			transmit_buf[0] = 0x83;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FOutputHoldingRegisters.FCount >= device_offset + registers_number)
		{
			transmit_buf[0] = 0x03;
			transmit_buf[1] = ((uint8_t)registers_number)*2;
			for(uint16_t i=0; i<registers_number; i++)
			{
				temp = GetUint16TagValue((ModbusDevice[device_adr].FOutputHoldingRegisters.FBegin)[device_offset+i]);
				transmit_buf[2+2*i] = (uint8_t)(temp>>8);
				transmit_buf[2+2*i+1] = (uint8_t)(temp & 0x00ff);
			}
						
			*(tr_buf_size) = 2 + 2*registers_number;
		}
		else
		{
			transmit_buf[0] = 0x83;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x84;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#endif
}

static void WriteSingleHoldingRegister(uint8_t *rec_buf, uint16_t rec_buf_size,
																			 uint16_t registers_offset,
																			 uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	uint16_t temp;
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_OUTPUT_HOLDING_REGISTERS_COUNT > registers_offset)
	{
			transmit_buf[0] = 0x06;
			transmit_buf[1] = rec_buf[1];
			transmit_buf[2] = rec_buf[2];
			temp = ((uint16_t)(rec_buf[3])<<8) | (rec_buf[4]);
			SetUint16TagValue(ModbusOutputHoldingRegisters[registers_offset], temp);
			temp = GetUint16TagValue(ModbusOutputHoldingRegisters[registers_offset]);
			transmit_buf[3] = (uint8_t)(temp>>8);
			transmit_buf[4] = (uint8_t)(temp & 0x00ff);
			*(tr_buf_size) = 5;
	}
	else
	{
			transmit_buf[0] = 0x86;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FOutputHoldingRegisters.FCount > device_offset)
		{
			transmit_buf[0] = 0x06;
			transmit_buf[1] = rec_buf[1];
			transmit_buf[2] = rec_buf[2];
			temp = ((uint16_t)(rec_buf[3])<<8) | (rec_buf[4]);
			TUint16Tag temp_tag = (ModbusDevice[device_adr].FOutputHoldingRegisters.FBegin)[device_offset];
			SetUint16TagValue(temp_tag, temp);
			temp = GetUint16TagValue(temp_tag);
			transmit_buf[3] = (uint8_t)(temp>>8);
			transmit_buf[4] = (uint8_t)(temp & 0x00ff);
			*(tr_buf_size) = 5;
		}
		else
		{
			transmit_buf[0] = 0x86;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x86;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#endif
}

static void WriteMultipleHoldingRegisters(uint16_t registers_offset, uint16_t registers_number,
																				  uint8_t *rec_buf, uint16_t rec_buf_size,
																			    uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	uint16_t temp;
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_OUTPUT_HOLDING_REGISTERS_COUNT >= registers_offset + registers_number)
	{
			for(uint16_t i = 0; i < registers_number; i++)
			{
				temp = ((uint16_t)(rec_buf[6+2*i])<<8) | (rec_buf[7+2*i]);
				SetUint16TagValue(ModbusOutputHoldingRegisters[registers_offset + i], temp);
			}
			
			transmit_buf[0] = 0x10;
			transmit_buf[1] = rec_buf[1];
			transmit_buf[2] = rec_buf[2];
			transmit_buf[3] = rec_buf[3];
			transmit_buf[4] = rec_buf[4];
			*(tr_buf_size) = 5;
	}
	else
	{
			transmit_buf[0] = 0x90;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FOutputHoldingRegisters.FCount >= device_offset + registers_number)
		{
			for(uint16_t i = 0; i < registers_number; i++)
			{
				temp = ((uint16_t)(rec_buf[6+2*i])<<8) | (rec_buf[7+2*i]);
				SetUint16TagValue((ModbusDevice[device_adr].FOutputHoldingRegisters.FBegin)[device_offset+i] , temp);
			}
			
			transmit_buf[0] = 0x10;
			transmit_buf[1] = rec_buf[1];
			transmit_buf[2] = rec_buf[2];
			transmit_buf[3] = rec_buf[3];
			transmit_buf[4] = rec_buf[4];
			*(tr_buf_size) = 5;
		}
		else
		{
			transmit_buf[0] = 0x90;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x90;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#endif
}

static void ReadInputRegisters(uint16_t registers_offset, uint16_t registers_number,
																				  uint8_t *rec_buf, uint16_t rec_buf_size,
																			    uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	uint16_t temp;
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_INPUT_REGISTERS_COUNT >= registers_offset + registers_number)
	{
			transmit_buf[0] = 0x04;
			transmit_buf[1] = ((uint8_t)registers_number)*2;
			for(uint16_t i=0; i<registers_number; i++)
			{
				temp = GetUint16TagValue(ModbusInputRegisters[registers_offset+i]);
				transmit_buf[2+2*i] = (uint8_t)(temp>>8);
				transmit_buf[2+2*i+1] = (uint8_t)(temp & 0x00ff);
			}
						
			*(tr_buf_size) = 2 + 2*registers_number;
	}
	else
	{
			transmit_buf[0] = 0x84;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FInputRegisters.FCount >= device_offset + registers_number)
		{
			transmit_buf[0] = 0x04;
			transmit_buf[1] = ((uint8_t)registers_number)*2;
			for(uint16_t i=0; i<registers_number; i++)
			{
				temp = GetUint16TagValue((ModbusDevice[device_adr].FInputRegisters.FBegin)[device_offset+i]);
				transmit_buf[2+2*i] = (uint8_t)(temp>>8);
				transmit_buf[2+2*i+1] = (uint8_t)(temp & 0x00ff);
			}
						
			*(tr_buf_size) = 2 + 2*registers_number;
		}
		else
		{
			transmit_buf[0] = 0x84;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x84;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#endif
}

static void ReadDiscreteCoils(uint16_t registers_offset, uint16_t registers_number,
																				  uint8_t *rec_buf, uint16_t rec_buf_size,
																			    uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_DISCRETE_COILS_COUNT > registers_offset + registers_number)
	{
			uint8_t byte_pos =0;
			uint16_t remaining_coils;
			uint16_t coils_cnr=0;
			while(coils_cnr<registers_number)
			{
				remaining_coils = registers_number - coils_cnr;
				if(8<remaining_coils)
				{
					transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDiscreteCoils, registers_offset+coils_cnr, 8);
					coils_cnr+=8;
				}
				else
				{
					transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDiscreteCoils, 
							                                              registers_offset+coils_cnr, (uint8_t)remaining_coils);
					coils_cnr+=remaining_coils;
				}
				byte_pos++;
			}
					
			transmit_buf[0] = 0x01;
			transmit_buf[1] = byte_pos;
			*(tr_buf_size) = 2 + byte_pos;
	}
	else
	{
			transmit_buf[0] = 0x81;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FDiscreteCoils.FCount >= device_offset + registers_number)
		{
			uint8_t byte_pos =0;
			uint16_t remaining_coils;
			uint16_t coils_cnr=0;
			while(coils_cnr<registers_number)
			{
				remaining_coils = registers_number - coils_cnr;
				if(8<remaining_coils)
				{
					transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDevice[device_adr].FDiscreteCoils.FBegin, 
																														device_offset+coils_cnr, 8);
					coils_cnr+=8;
				}
				else
				{
					transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDevice[device_adr].FDiscreteCoils.FBegin, 
							                                              device_offset+coils_cnr, (uint8_t)remaining_coils);
					coils_cnr+=remaining_coils;
				}
				byte_pos++;
			}
					
			transmit_buf[0] = 0x01;
			transmit_buf[1] = byte_pos;
			*(tr_buf_size) = 2 + byte_pos;
		}
		else
		{
			transmit_buf[0] = 0x81;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x81;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#endif
}

static void WriteSingleOutputCoil(uint8_t *rec_buf, uint16_t rec_buf_size,
																			 uint16_t registers_offset,
																			 uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_DISCRETE_COILS_COUNT > registers_offset)
	{
			int8_t correct_par = 0;
			if(0x00 == rec_buf[4])
			{
				if(0x00 == rec_buf[3])
					correct_par = 1; 
				if(0xff == rec_buf[3])
					correct_par = -1;
			}
			if(0 != correct_par)
			{
				if(0x00 == rec_buf[3])
					*(ModbusDiscreteCoils[registers_offset].FValue) = false;
				if(0xff == rec_buf[3])
					*(ModbusDiscreteCoils[registers_offset].FValue) = true;
						
				transmit_buf[0] = 0x05;
				transmit_buf[1] = rec_buf[1];
				transmit_buf[2] = rec_buf[2];
				transmit_buf[3] = rec_buf[3];
				transmit_buf[4] = rec_buf[4];
				*(tr_buf_size) = 5;
			}
			else
			{
				transmit_buf[0] = 0x85;
				transmit_buf[1] = 0x03;
				*(tr_buf_size) = 2;
			}
	}
	else
	{
			transmit_buf[0] = 0x85;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FDiscreteCoils.FCount > device_offset)
		{
			int8_t correct_par = 0;
			if(0x00 == rec_buf[4])
			{
				if(0x00 == rec_buf[3])
					correct_par = 1; 
				if(0xff == rec_buf[3])
					correct_par = -1;
			}
			if(0 != correct_par)
			{
				if(0x00 == rec_buf[3])
					*((ModbusDevice[device_adr].FDiscreteCoils.FBegin)[device_offset].FValue) = false;
				if(0xff == rec_buf[3])
					*((ModbusDevice[device_adr].FDiscreteCoils.FBegin)[device_offset].FValue) = true;
						
				transmit_buf[0] = 0x05;
				transmit_buf[1] = rec_buf[1];
				transmit_buf[2] = rec_buf[2];
				transmit_buf[3] = rec_buf[3];
				transmit_buf[4] = rec_buf[4];
				*(tr_buf_size) = 5;
			}
			else
			{
				transmit_buf[0] = 0x85;
				transmit_buf[1] = 0x03;
				*(tr_buf_size) = 2;
			}
		}
		else
		{
			transmit_buf[0] = 0x85;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x85;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
	}
	#endif
}

static void WriteMultipleOutputCoils(uint16_t registers_offset, uint16_t registers_number,
																		 uint8_t *rec_buf, uint16_t rec_buf_size,
																		 uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_DISCRETE_COILS_COUNT >= registers_offset + registers_number)
	{
		uint8_t byte_pos =0;
		uint16_t remaining_coils;
		//uint16_t coils_cnr=0;
		while(byte_pos<rec_buf[5])
		{
			remaining_coils = registers_number - byte_pos*8;
			if(8<remaining_coils)
				SetBoolValueBitmask(&(ModbusDiscreteCoils[registers_offset+8*byte_pos]), rec_buf[6+byte_pos], 8);
			else
				SetBoolValueBitmask(&(ModbusDiscreteCoils[registers_offset+8*byte_pos]), rec_buf[6+byte_pos], (uint8_t)remaining_coils);
			byte_pos++;
		}
		
		transmit_buf[0] = 0x0f;
		transmit_buf[1] = rec_buf[1];
		transmit_buf[2] = rec_buf[2];
		transmit_buf[3] = rec_buf[3];
		transmit_buf[4] = rec_buf[4];
		*(tr_buf_size) = 5;
	}
	else
	{
		transmit_buf[0] = 0x8f;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FDiscreteCoils.FCount > device_offset)
		{
			uint8_t byte_pos =0;
			uint16_t remaining_coils;
			//uint16_t coils_cnr=0;
			while(byte_pos<rec_buf[5])
			{
				remaining_coils = registers_number - byte_pos*8;
				if(8<remaining_coils)
					SetBoolValueBitmask(&(ModbusDevice[device_adr].FDiscreteCoils.FBegin[device_offset+8*byte_pos]), 
																rec_buf[6+byte_pos], 8);
				else
					SetBoolValueBitmask(&(ModbusDevice[device_adr].FDiscreteCoils.FBegin[device_offset+8*byte_pos]), 
																rec_buf[6+byte_pos], (uint8_t)remaining_coils);
				byte_pos++;
			}
			
			transmit_buf[0] = 0x0f;
			transmit_buf[1] = rec_buf[1];
			transmit_buf[2] = rec_buf[2];
			transmit_buf[3] = rec_buf[3];
			transmit_buf[4] = rec_buf[4];
			*(tr_buf_size) = 5;
		}
		else
		{
			transmit_buf[0] = 0x8f;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x8f;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#endif
}

static void ReadDiscreteInputs(uint16_t registers_offset, uint16_t registers_number,
																				  uint8_t *rec_buf, uint16_t rec_buf_size,
																			    uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	#ifndef DEVICE_ORIENTED_MODBUS_MAP
	if(MODBUS_DISCRETE_INPUTS_COUNT >= registers_offset + registers_number)
	{
		uint8_t byte_pos =0;
		uint16_t remaining_coils;
		uint16_t coils_cnr=0;
		while(coils_cnr<registers_number)
		{
			remaining_coils = registers_number - coils_cnr;
			if(8<remaining_coils)
			{
				transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDiscreteInputs, registers_offset+coils_cnr, 8);
				coils_cnr+=8;
			}
			else
			{
				transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDiscreteInputs,
																													registers_offset+coils_cnr, (uint8_t)remaining_coils);
				coils_cnr+=remaining_coils;
			}
			byte_pos++;
		}
					
		transmit_buf[0] = 0x02;
		transmit_buf[1] = byte_pos;
		*(tr_buf_size) = 2 + byte_pos;
	}
	else
	{
		transmit_buf[0] = 0x82;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#else
	uint16_t device_adr = registers_offset/100;
	uint16_t device_offset = registers_offset - 100*device_adr;
	if(13>device_adr)
	{
		if(ModbusDevice[device_adr].FDiscreteInputs.FCount >= device_offset + registers_number)
		{
			uint8_t byte_pos =0;
			uint16_t remaining_coils;
			uint16_t coils_cnr=0;
			while(coils_cnr<registers_number)
			{
				remaining_coils = registers_number - coils_cnr;
				if(8<remaining_coils)
				{
					transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDevice[device_adr].FDiscreteInputs.FBegin, 
																														device_offset+coils_cnr, 8);
					coils_cnr+=8;
				}
				else
				{
					transmit_buf[2+byte_pos] = GetBoolTagValueBitmask(ModbusDevice[device_adr].FDiscreteInputs.FBegin, 
																														device_offset+coils_cnr, (uint8_t)remaining_coils);
					coils_cnr+=remaining_coils;
				}
				byte_pos++;
			}
					
			transmit_buf[0] = 0x02;
			transmit_buf[1] = byte_pos;
			*(tr_buf_size) = 2 + byte_pos;
		}
		else
		{
			transmit_buf[0] = 0x82;
			transmit_buf[1] = 0x02;
			*(tr_buf_size) = 2;
		}
	}
	else
	{
		transmit_buf[0] = 0x82;
		transmit_buf[1] = 0x02;
		*(tr_buf_size) = 2;
	}
	#endif
}


void MakeModbusPdu(uint8_t *rec_buf, uint16_t rec_buf_size, uint8_t *transmit_buf, uint16_t *tr_buf_size)
{
	uint8_t func_code;
	uint16_t registers_number;
	uint16_t registers_offset;
	if(0<rec_buf_size)
	{
		func_code = rec_buf[0];
		switch(func_code)
		{
			case 0x03:   // read output holding registers
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
				registers_number = (uint16_t)(rec_buf[3])<<8 | rec_buf[4];
				
				ReadHoldingRegisters(registers_offset, registers_number, transmit_buf, tr_buf_size);
				
				break;
			case 0x06:  // write single output holding register
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
				
				WriteSingleHoldingRegister(rec_buf, rec_buf_size, registers_offset, transmit_buf, tr_buf_size);
			
				break;
			case 0x10:   // write multiple output holding registers
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
				registers_number = (uint16_t)(rec_buf[3])<<8 | rec_buf[4];
			
				WriteMultipleHoldingRegisters(registers_offset, registers_number, rec_buf, 
																			rec_buf_size, transmit_buf, tr_buf_size);
				break;
			case 0x04:  // read input registers
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
				registers_number = (uint16_t)(rec_buf[3])<<8 | rec_buf[4];
				
				ReadInputRegisters(registers_offset, registers_number, rec_buf, 
													 rec_buf_size, transmit_buf, tr_buf_size);
				break;
			case 0x01:  // read discrete coil
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
				registers_number = (uint16_t)(rec_buf[3])<<8 | rec_buf[4];
			
			  ReadDiscreteCoils(registers_offset, registers_number, rec_buf, 
													 rec_buf_size, transmit_buf, tr_buf_size);
			
				break;
			case 0x05:   // write single output coil
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
			
				WriteSingleOutputCoil(rec_buf, rec_buf_size, registers_offset, transmit_buf, tr_buf_size);
			
				break;
			case 0x0f:  // write multiple output coils
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
				registers_number = (uint16_t)(rec_buf[3])<<8 | rec_buf[4];
			
				WriteMultipleOutputCoils(registers_offset, registers_number, rec_buf, 
																 rec_buf_size, transmit_buf, tr_buf_size);
			
				break;
			case 0x02: // read discrete inputs
				registers_offset = (uint16_t)(rec_buf[1])<<8 | rec_buf[2];  // parse offset and count
				registers_number = (uint16_t)(rec_buf[3])<<8 | rec_buf[4];
				
				ReadDiscreteInputs(registers_offset, registers_number, rec_buf, 
													 rec_buf_size, transmit_buf, tr_buf_size);
				
				break;
			default:
				// generate exception response (illegal func code)
				transmit_buf[0] = func_code | 0x80;
			  transmit_buf[1] = 0x01;
				*(tr_buf_size) = 2;
				break;
		}
	}
}
