

#include "RTC/RTC.h"

RTC_HandleTypeDef GRtcHandle;

void RTC_Init(void)
{
	/*##-1- Configure the RTC peripheral #######################################*/
  /* Configure RTC prescaler and RTC data registers */
  /* RTC configured as follow:
      - Hour Format    = Format 12
      - Asynch Prediv  = Value according to source clock
      - Synch Prediv   = Value according to source clock
      - OutPut         = Output Disable
      - OutPutPolarity = High Polarity
      - OutPutType     = Open Drain */
  __HAL_RTC_RESET_HANDLE_STATE(&GRtcHandle);
  GRtcHandle.Instance = RTC;
  GRtcHandle.Init.HourFormat     = RTC_HOURFORMAT_12;
  GRtcHandle.Init.AsynchPrediv   = RTC_ASYNCH_PREDIV;
  GRtcHandle.Init.SynchPrediv    = RTC_SYNCH_PREDIV;
  GRtcHandle.Init.OutPut         = RTC_OUTPUT_DISABLE;
  GRtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  GRtcHandle.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;

  if(HAL_RTC_Init(&GRtcHandle) != HAL_OK)
  {
    /* Initialization Error */
    while(1)
		{
			HAL_Delay(10);
		}
  }
}

