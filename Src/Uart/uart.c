
#include "Uart/uart.h"
#include "GraphicTask/MeasureWin.h"
#include <string.h>
#include <stdlib.h>


#define DE_PORT GPIOG
#define DE_PIN GPIO_PIN_6


/* Buffer used for transmission */
static uint8_t aTxBuffer[] = {'I', 'N', 'J', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

/* Buffer used for reception */
static uint8_t aRxBuffer[RXBUFFERSIZE];

static UART_HandleTypeDef UartHandle;

static uint8_t UartReady;

TUART_Error 	GUART_Error;
TMessageType 	GUartMessageType;  
TProtocol			GMessage;
uint8_t GDelay_ON;

static TUART_Error SendAndVerify(void)	;
static void sendUart(UART_HandleTypeDef* uart, TUART_Error* res);
static uint8_t crc8d5(uint8_t *pcBlock, unsigned int len, uint8_t *crc);



uint8_t UartInit(void)
{
	/*##-1- Configure the UART peripheral ######################################*/
  UartHandle.Instance        = USARTx;
  UartHandle.Init.BaudRate   = 9600;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX_RX;
  UartHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
  {
    return 0;
  }  
  if(HAL_UART_Init(&UartHandle) != HAL_OK)
  {
    return 0;
  }	
		
	GMeasurementStart = 0;
	GUartMessageType = NoMessageDetected;
	UartReady = RESET;
	GDelay_ON = 0;
	
	return 1;
}

void UART_Thread(void const *argument)
{
	char str[50] = "";
	TUART_Error 	res = UART_OK;
	UartInit();
	osDelay(4000);
	
	while(1)
	{
		if(GParamChanged == 1)				// If Current or Time changed.
		{
			GMessage.FType = SetHold1Current;				// Set Current.	 
			GMessage.FParameter = GCurrent*93;			// 1 A = 93.
			sendUart(&UartHandle, &res);
			GUART_Error = res;
			
			GMessage.FType = ReadHold1Current;			// Read Current. 
			GMessage.FParameter = GCurrent*93;			// 1 A = 93.
			sendUart(&UartHandle, &res);
			GUART_Error = res;
			
			GMessage.FType = SetHold1Time;		// 	Set Time.
			GMessage.FParameter = GTime;			// in ms.
			sendUart(&UartHandle, &res);
			GUART_Error = res;
			
			GMessage.FType = ReadHold1Time;		// Read Time. 
			GMessage.FParameter = GTime;			// in ms.
			sendUart(&UartHandle, &res);
			GUART_Error = res;
			
			GParamChanged = 0;
		}
		
		if(GMeasurementStart == 1)			// If Measurements started.
		{
			GMessage.FType = StartSingleImpulse;				 
			GMessage.FParameter = 0;	
			res = SendAndVerify();
			if(res  == UART_OK)
			{															
				sprintf(str ,"Param START sended");									
				AddLog(str);
			}
			else
			{																			
				sprintf(str ,"Param START send 1: UART err %d", res);									
				AddLog(str);													
			}
			GUART_Error = res;
			osDelay(10000 + GTime);		// Wait for cooling. 		// Ti: 1000 + COOLING_DELAY_COEF*GTime !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						
			GMeasurementStart = 0;
		}	
		
		osDelay(10);							
	}
}		

TUART_Error SendAndVerify(void)	
{
	uint8_t	timoutCounter = 0;
	uint8_t crcPosition = 0;
	
	// Form message:
	aTxBuffer[4] = GMessage.FType;
	
	uint8_t messageSize;
	if(GMessage.FType == SetHold1Current || GMessage.FType == SetHold1Time)
	{
		aTxBuffer[5] = (uint8_t)(GMessage.FParameter >> 24);
		aTxBuffer[6] = (uint8_t)(GMessage.FParameter >> 16);
		aTxBuffer[7] = (uint8_t)(GMessage.FParameter >> 8);
		aTxBuffer[8] = (uint8_t)(GMessage.FParameter);
		aTxBuffer[9] = 0;
		aTxBuffer[9] = crc8d5(aTxBuffer, 9, &(aTxBuffer[9]));
		messageSize = 10;
	}
	else
	{
		aTxBuffer[5] = 0;
		aTxBuffer[5] = crc8d5(aTxBuffer, 5, &(aTxBuffer[5]));
		messageSize = 6;
	}
	// Choose answer size:
	uint8_t	answerSize;
	if(GMessage.FType == ReadHold1Current || GMessage.FType == ReadHold1Time)
		answerSize = 9;
	else
		answerSize = 5;
	
	
	//	Put UART peripheral in transmission process =========== : 	
	UartReady = RESET;
	
	if(HAL_UART_Transmit_IT(&UartHandle, (uint8_t*)aTxBuffer, messageSize)!= HAL_OK)			// Start the transmission process.
	{
		return TX_ERR;
	}
	
	while (UartReady != SET)					// Wait for the end of the transfer.
	{
		osDelay(2);
		if(++timoutCounter == 0)				// if 500 ms.
		{			
			// Disable the UART Transmit Complete Interrupt ==:
			__HAL_UART_DISABLE_IT(&UartHandle, UART_IT_TC);
      // Disable the UART Error Interrupt ==:
      __HAL_UART_DISABLE_IT(&UartHandle, UART_IT_ERR);
			UartHandle.State = HAL_UART_STATE_READY;
			return TIMEOUT_ERR;
		}
	}
	timoutCounter = 0;
	
	UartReady = RESET;
	
	//		Put UART peripheral in reception process =========== : 
	if(HAL_UART_Receive_IT(&UartHandle, (uint8_t *)aRxBuffer, answerSize) != HAL_OK)
	{
		return RX_ERR;
	}
	
	while (UartReady != SET)					// Wait for the end of the receiving.
	{
		osDelay(2);
		if(++timoutCounter == 0)				// if 500 ms.
		{
			// Disable the UART Rx Interrupt ==:			
			__HAL_UART_DISABLE_IT(&UartHandle, UART_IT_RXNE);
			// Disable the UART Parity Error Interrupt ==:
      __HAL_UART_DISABLE_IT(&UartHandle, UART_IT_PE);
      // Disable the UART Error Interrupt ==:
      __HAL_UART_DISABLE_IT(&UartHandle, UART_IT_ERR);			
			UartHandle.State = HAL_UART_STATE_READY;
			return TIMEOUT_ERR;
		}
	}
	timoutCounter = 0;
	
	
	//		Verification process =========== :
	if(aRxBuffer[0] != 'A' || aRxBuffer[1] != 'N' || aRxBuffer[2] != 'S' || aRxBuffer[3] != (uint8_t)GMessage.FType)
	{
		return VERIFY_ERR;
	}
	
	// Check parameter:
	if(GMessage.FType == ReadHold1Current || GMessage.FType == ReadHold1Time)
	{
		uint32_t temp = ((((uint32_t)aRxBuffer[4])<<24UL) | (((uint32_t)aRxBuffer[5])<<16UL) | (((uint32_t)aRxBuffer[6])<<8UL) | ((uint32_t)aRxBuffer[7]));
		if(GMessage.FParameter != temp)
		{
			return VERIFY_ERR;
		}
		crcPosition = 8;
	}
	else
	{
		crcPosition = 4;
	}

	// Check CRC:
	uint8_t crc = 0;
	crc = crc8d5(aRxBuffer, crcPosition, &crc);
	if( crc != aRxBuffer[crcPosition] )
	{
		return VERIFY_ERR;
	}
	
	return UART_OK;
}
		
static void sendUart(UART_HandleTypeDef* uart, TUART_Error* res)	
{	
	char str[50] = "";
	for(uint8_t i=0; i<3; i++)								
	{																					
		*res = SendAndVerify();			// x 3 if error;	
		if(*res != UART_OK)											
		{																				
			if(i>1)																
			{																			
				RELOAD_UART(uart);						
			}																			
			sprintf(str ,"Param %d send %d: UART err %d", GMessage.FParameter, i+1, *res);			
			AddLog(str);													
		}																				
		else																		
		{																				
			sprintf(str ,"Param %d sended", GMessage.FParameter);									
			AddLog(str);													
			break;																
		}																				
	}
}


static uint8_t crc8d5(uint8_t *pcBlock, unsigned int len, uint8_t *crc)
{
	unsigned int i;
 
  while (len--)
  {
    *crc ^= *pcBlock++;
 
    for (i = 0; i < 8; i++)
        *crc = *crc & 0x80 ? (*crc << 1) ^ 0xd5 : *crc << 1;
  }
  return *crc;
}
			


/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of DMA Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: trasfer complete*/
  UartReady = SET;  
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  /* Set transmission flag: trasfer complete*/
  UartReady = SET;  
}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
//void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
//{
    //Error_Handler();
//}



/**
  * @brief  This function handles UART interrupt request.  
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA  
  *         used for USART data transmission     
  */
void USARTx_IRQHandler(void)
{
  HAL_UART_IRQHandler(&UartHandle);
}






