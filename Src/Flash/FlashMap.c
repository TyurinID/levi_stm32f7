
#include "Flash/FlashMap.h"

TFlashSavingRegister GFlashSavingRegister[FLASHMAP_COUNT]; // flash data map


		//========================TI:
void EraseFlash(void)
{
	taskENTER_CRITICAL();
	// erase
  HAL_FLASH_Unlock();

  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/


  /* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
     you have to make sure that these data are rewritten before they are accessed during code
     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
     DCRST and ICRST bits in the FLASH_CR register. */

	
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_SR_ERSERR | FLASH_SR_BSY);

	FLASH_Erase_Sector(FLASH_SECTOR_7, VOLTAGE_RANGE_3);
	
	HAL_FLASH_Lock();
	
	taskEXIT_CRITICAL();
}		

void WriteFlash(uint16_t InjNumber)
{	
	taskENTER_CRITICAL();
	// erase
  HAL_FLASH_Unlock();
	
	uint32_t Address = FLASH_USER_START_ADDR + InjNumber*sizeof(TInjector);		// 
	
	uint64_t data_in[14];
	data_in[0] = (uint64_t)(GInjector.InjectorCode);
	data_in[1] = (uint64_t)(GInjector.ImageType);
	for(uint8_t i = 0; i < 5; i ++)
	{
		data_in[2+i*2] = (uint64_t)(GInjector.ParamNominal[i]*10000);
		data_in[3+i*2] = (uint64_t)(GInjector.ParamTolerance[i]*10000);
	}
	data_in[12] = (uint64_t)(GInjector.Current);
	data_in[13] = (uint64_t)(GInjector.Time);
	
	
  for(uint16_t i = 0; i<14; i++)
  {
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, data_in[i] ); // != HAL_OK)
		Address += 4;
  }
	HAL_FLASH_Lock();
	
	taskEXIT_CRITICAL();
}
		

TInjector	ReadFlash(uint32_t InjCode)
{
	uint32_t temp;
	TInjector GTempInjector;
	GTempInjector.InjectorCode = 0;
			
	for(uint32_t addr = FLASH_USER_START_ADDR; addr < (FLASH_USER_START_ADDR + InjectorMaxNum*sizeof(TInjector)); addr+=sizeof(TInjector))
	{
		temp = *((__IO uint32_t *)(addr));
		if(InjCode == temp)
		{
			GTempInjector.InjectorCode = InjCode;
			GTempInjector.ImageType = *( (__IO uint32_t *)(addr+4) );
			for(uint8_t i = 0; i < 5; i ++)
			{
				GTempInjector.ParamNominal[i] = ((float)*( (__IO uint32_t *)(addr+8 +i*8)))/10000;
				GTempInjector.ParamTolerance[i] = ((float)*( (__IO uint32_t *)(addr+12 +i*8)))/10000;
			}
			GTempInjector.Current =  *( (__IO uint32_t *)(addr+48) );
			GTempInjector.Time = *( (__IO uint32_t *)(addr+52) );
			break;
		}
	}
	return GTempInjector;
}
		//========================TI.
