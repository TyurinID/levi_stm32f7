

#include "ModbusTCP/ModbusTCP.h"

#include "TagSystem/ModbusPDU.h"

static uint8_t transmit_buf[256];
static uint16_t tr_buf_size;


struct netif gnetif; /* network interface structure */

/**
  * @brief  Initializes the lwIP stack
  * @param  None
  * @retval None
  */
static void Netif_Config(void)
{
  struct ip_addr ipaddr;
  struct ip_addr netmask;
  struct ip_addr gw;	
  
  /* IP address setting */
  IP4_ADDR(&ipaddr, IP_ADDR0, IP_ADDR1, IP_ADDR2, IP_ADDR3);
  IP4_ADDR(&netmask, NETMASK_ADDR0, NETMASK_ADDR1 , NETMASK_ADDR2, NETMASK_ADDR3);
  IP4_ADDR(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);
  
  /* - netif_add(struct netif *netif, struct ip_addr *ipaddr,
  struct ip_addr *netmask, struct ip_addr *gw,
  void *state, err_t (* init)(struct netif *netif),
  err_t (* input)(struct pbuf *p, struct netif *netif))
  
  Adds your network interface to the netif_list. Allocate a struct
  netif and pass a pointer to this structure as the first argument.
  Give pointers to cleared ip_addr structures when using DHCP,
  or fill them with sane numbers otherwise. The state pointer may be NULL.
  
  The init function pointer must point to a initialization function for
  your ethernet netif interface. The following code illustrates it's use.*/
  
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);
  
  /*  Registers the default network interface. */
  netif_set_default(&gnetif);
  
  if (netif_is_link_up(&gnetif))
  {
    /* When the netif is fully configured this function must be called.*/
    netif_set_up(&gnetif);
  }
  else
  {
    /* When the netif link is down this function must be called */
    netif_set_down(&gnetif);
  }
}

static void ModbusAppCall(struct netconn *conn) 
{
	struct netbuf *inbuf;
  err_t recv_err;
  uint8_t* buf;
  u16_t buflen;
	uint16_t incoming_packet_length;
	uint16_t outcoming_packet_length;
  
  
  /* Read the data from the port, blocking if nothing yet there. 
   We assume the request (the part we care about) is in one netbuf */
  recv_err = netconn_recv(conn, &inbuf);
	

	if (recv_err == ERR_OK)
  {
    if (netconn_err(conn) == ERR_OK) 
    {
      netbuf_data(inbuf, (void**)&buf, &buflen);
			
			
				// generate adu
			transmit_buf[0] = buf[0];  // copy message id
			transmit_buf[1] = buf[1];
			transmit_buf[2] = buf[2];  // copy protocol id
			transmit_buf[3] = buf[3];
			//transmit_buf[4] = 0;
			//transmit_buf[5] = 5;                  // set data length
			transmit_buf[6] = buf[6];  // copy unit id
			
				// generate pdu
			incoming_packet_length = ((uint16_t)(buf[4]))<<8 | (uint16_t)(buf[5]);
			MakeModbusPdu(&(buf[7]), incoming_packet_length, &(transmit_buf[7]), &outcoming_packet_length);
			
			transmit_buf[4] = (uint8_t)((outcoming_packet_length+1) >> 8);
			transmit_buf[5] = (uint8_t)((outcoming_packet_length+1) & 0x00ff);
			tr_buf_size = 7 + outcoming_packet_length;
			
			netconn_write(conn, (const uint8_t*)(transmit_buf), (size_t)tr_buf_size, NETCONN_NOCOPY);
			
    }
	}
	/* Close the connection (server closes in HTTP) */
  netconn_close(conn);
  
  /* Delete the buffer (netconn_recv gives us ownership,
   so we have to make sure to deallocate the buffer) */
  netbuf_delete(inbuf);
}


void ModbusTCPThread(void const *argument)
{
	struct netconn *conn, *newconn;
  err_t err, accept_err;
	
	/* Create tcp_ip stack thread */
  tcpip_init(NULL, NULL);
  
  /* Initialize the LwIP stack */
  Netif_Config();
	
	osDelay(100);

	
	/* Create a new TCP connection handle */
  conn = netconn_new(NETCONN_TCP);
  
  if (conn!= NULL)
  {
    /* Bind to port 502 (Modbus-tcp) with default IP address */
    err = netconn_bind(conn, NULL, 502);
    
    if (err == ERR_OK)
    {
      /* Put the connection into LISTEN state */
      netconn_listen(conn);
  
      while(1) 
      {
        /* accept any icoming connection */
        accept_err = netconn_accept(conn, &newconn);
        if(accept_err == ERR_OK)
        {
          /* serve connection */
          ModbusAppCall(newconn);

          /* delete connection */
          netconn_delete(newconn);
        }
      }
    }
  }
		
}





