
#include "GUI.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"

#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/GraphicCommon.h"
#include "GraphicTask/GraphKeyboard.h"
#include "GraphicTask/GraphError.h"
#include "GraphicTask/MeasureWin.h"
#include "GraphicTask/GraphWarning.h"
#include "GraphicTask/LogPage.h"

#include "uSD/uSD_API.h"
#include "Flash/FlashMap.h"
#include "Uart/uart.h"

#include <math.h>
#include <string.h>

extern GUI_CONST_STORAGE GUI_BITMAP bmlogoLevi;
uint8_t GToDrawFLG = 1;										// Draw new window right now.

static uint8_t btn_state;
static uint32_t btn_state_prev;
static uint8_t touched_prev;					// Is touched previously?.
static void ButtonInit(void);					// Configure external button "Start".
static void LogoInit(void);						// Draw logo "STARDEX LEVI".
static void GreetingStardex(void);		// Show "WELCOM TO STARDEX" .
static void ButtonProcess(void);			// Check the button state.

#define BTN_STATE GPIOB->IDR & GPIO_PIN_4

void GraphicThread(void const *argument)
{
	__IO TS_StateTypeDef  ts;
 
	osDelay(1000);
	ButtonInit();
	BSP_TS_Init(420, 272);
	LoadBase();								// Load Inj Database fron uSD to Flash, if there are any Differences.
	osDelay(1000);
	LogoInit();								// Draw logo "STARDEX LEVI". 
	//=============== init structures:
	InitializeScreenSectors();								 
	MeasureWin_Init();
	KeyBoard_Init();
	WarningWin_Init();
	ErrorWin_Init();
	LogPage_Init();
		
	if(SD_Error != FR_OK)			// Check, is SD Card normaly read.
	{
		sprintf(GWarningWin.Text, "SD Card not found! \n Do you want to erase Flash?");
		GWarningWin.Active = 2;		// Turn ON WarningWin process 2.
		GToDrawFLG = 1;
		SD_Error = FR_OK;					// Reset Error.
	}	
	else
		GreetingStardex();				// Show "WELCOM TO STARDEX". 
	
	touched_prev = 0;					// Is touched previously?
	
  while(1)
	{
		BSP_TS_GetState((TS_StateTypeDef *)&ts);
		uint8_t touchDetected = ts.touchDetected;		
		
		//=========== btn cheking:
		if(GSelectedInjector.InjectorCode != 0)				// If injector is selected.
			ButtonProcess();
			
		//=========== error checking:
		if(GUART_Error != UART_OK)
		{
			GUI_SetColor(GUI_BLACK);
			GUI_FillRect(476,2,478,4);
		}
			
		//=========== draw windows:
		if(GToDrawFLG)					// Only if something in graphic has changed.
		{
			if(GErrorWin.Active)
				ErrorWin_Draw();
			else	if(GWarningWin.Active)
				WarningWin_Draw();
			else	if(GKeyBoard.Active)
				KeyBoard_Draw();
			else	if(gLogWin.Active)
				LogPage_Draw();
			else	if(GMeasureWin.Active)
				MeasureWin_Draw();
			GToDrawFLG = 0;
		}
		
		//=========== window process:
		if(touchDetected == 1 && touched_prev == 0)		// If just clicked:
		{
			if(GErrorWin.Active)
				ErrorWin_Process(ts);
			else	if(GWarningWin.Active == 2)
				WarningWin_Process2(ts);
			else	if(GWarningWin.Active == 1)
				WarningWin_Process(ts);
			else	if(GWarningWin.Active == 3)
				WarningWin_Process3(ts);
			else	if(GKeyBoard.Active)
				KeyBoard_Process(ts);
			else	if(gLogWin.Active)
				LogPage_Process(ts);
			else	if(GMeasureWin.Active)
				MeasureWin_Process(ts);			
		}		
		
		//============= injector changing checking:
		if(GInjChanged == 1)									// New injector selected from database.
		{
			GSelectedInjector = ReadFlash(GSelectedInj);		// Read data about new Injector.
			if(GSelectedInjector.InjectorCode == 0)
			{	
				sprintf(GWarningWin.Text, "Injector Code not Found");
				GWarningWin.Active = 3;							// Activate window "Can't find..".	
			}
			else
			{
				GCurrent = GSelectedInjector.Current;
				GTime = GSelectedInjector.Time;
				GParamChanged = 1;
			}
			GToDrawFLG = 1;					// Redraw MeasureWin or Message "Injector code not found!".
			GInjChanged = 0;
		}
		
		touched_prev = touchDetected;
				
		osDelay(20);						// 20 ms period.
	}
}

static void ButtonInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	__GPIOB_CLK_ENABLE();
	
	// Configure peripheral GPIO =========:  
  GPIO_InitStruct.Pin       = GPIO_PIN_4;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT ;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
//  GPIO_InitStruct.Alternate = USARTx_TX_AF;

  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	btn_state = 0xff;
	btn_state_prev = 0xff;
}

void LogoInit(void)
{	
	// Background color:
	GUI_SetBkColor(GUI_BLUE_00);
  GUI_Clear();
	GUI_SelectLayer(0);
	GUI_DrawBitmap(&bmlogoLevi, 2  , 5);		// Draw logo "STARDEX LEVI" 
}

void GreetingStardex(void)		//	Show "WELCOME TO STARDEX" 
{	
	char str[18] = {'W','E','L','C','O','M','E',' ','T','O',' ','S','T','A','R','D','E','X'};
	uint32_t blue_fade_in[9] = { GUI_TEAL_00, GUI_TEAL_01, GUI_TEAL_02, GUI_TEAL_03, GUI_TEAL_04, GUI_TEAL_05, GUI_TEAL_06, GUI_TEAL_07, GUI_TEAL_08 };
	
	uint8_t i = 0;
	for (i = 0; i < 8; i++)
	{
		GUI_SetColor(blue_fade_in[i]);
		for(uint8_t i = 0; i<2*SCREEN_SECTORS_HORIZONTAL_CNT; i++)
		{
			hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
													 GScreen_sector[i].FYBegin, 
													 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin-X_SECTORS_OVERLAP, 
													 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
			GUI_MEMDEV_Select(hMem);
			GUI_Clear();

			GUI_SetFont(GUI_FONT_32_1);
			GUI_SetTextMode(GUI_TM_TRANS);
			GUI_SetTextAlign(GUI_TA_CENTER);
					
			GUI_DispStringAt(str, 240, 132);
							
			GUI_MEMDEV_Select(0);
			GUI_MEMDEV_CopyToLCD(hMem);
			GUI_MEMDEV_Delete(hMem);
		}
		
		osDelay(150);
	}
	osDelay(500);
}



static void ButtonProcess(void)			// Check the button state.
{		
	static uint8_t period_timer = 0;
	if(++period_timer > 4)
	{
		period_timer = 0;
		btn_state = BTN_STATE;
		
		if(0==btn_state && 0!=btn_state_prev)
		{
			GMeasurementStart = 1;
			osDelay(500);					// For UART.
			DrawWAIT();
			while(GMeasurementStart == 1);			
			GToDrawFLG = 1;					// Redraw display.	
		}
		
		btn_state_prev = btn_state;
	}
}
/*************************** End of file ****************************/
