
#include "GraphicTask/GraphKeyboard.h"
#include "GraphicTask/MeasureWin.h"

#define BCKSP_POS 10
#define ENTER_POS 11

TKeyBoard GKeyBoard;

static GUI_RECT key[15];
static uint32_t dummy_value;

void KeyBoard_Init(void)
{
	GKeyBoard.Active = 0;
	GKeyBoard.digits_length = 0;
	GKeyBoard.Target = &dummy_value;
	GKeyBoard.FMin = 0;
	GKeyBoard.Coord[0] = 20;
	GKeyBoard.Coord[1] = 170;
	GKeyBoard.Coord[2] = 80;
	GKeyBoard.Coord[3] = 120;
	
	// temp
	GKeyBoard.digits[0] = 1;
	GKeyBoard.digits[1] = 2;
	GKeyBoard.digits[2] = 3;
	
	key[1].x0 = 195;
	key[1].y0 = 60;
	key[1].x1 = 283;
	key[1].y1 = 107;
	
	key[2].x0 = 286;
	key[2].y0 = 60;
	key[2].x1 = 373;
	key[2].y1 = 107;
	
	key[3].x0 = 376;
	key[3].y0 = 60;
	key[3].x1 = 465;
	key[3].y1 = 107;
	
	key[4].x0 = 195;
	key[4].y0 = 110;
	key[4].x1 = 283;
	key[4].y1 = 157;

	key[5].x0 = 286;
	key[5].y0 = 110;
	key[5].x1 = 373;
	key[5].y1 = 157;
	
	key[6].x0 = 376;
	key[6].y0 = 110;
	key[6].x1 = 465;
	key[6].y1 = 157;
	
	key[7].x0 = 195;
	key[7].y0 = 160;
	key[7].x1 = 283;
	key[7].y1 = 207;
	
	key[8].x0 = 286;
	key[8].y0 = 160;
	key[8].x1 = 373;
	key[8].y1 = 207;
	
	key[9].x0 = 376;
	key[9].y0 = 160;
	key[9].x1 = 465;
	key[9].y1 = 207;
	
	key[0].x0 = 286;
	key[0].y0 = 210;
	key[0].x1 = 373;
	key[0].y1 = 257;
	
	key[BCKSP_POS].x0 = 195;
	key[BCKSP_POS].y0 = 210;
	key[BCKSP_POS].x1 = 283;
	key[BCKSP_POS].y1 = 257;
	
	key[ENTER_POS].x0 = 376;
	key[ENTER_POS].y0 = 210;
	key[ENTER_POS].x1 = 465;
	key[ENTER_POS].y1 = 257;
}


void KeyBoard_Process(__IO TS_StateTypeDef  ts)
{
	uint8_t key_pressed = 0xff;
	for(uint8_t i=0; i<12; i++)
	{
		if(key[i].x0 < ts.touchX[0] && 
			 key[i].x1 > ts.touchX[0] &&
		   key[i].y0 < ts.touchY[0] &&
			 key[i].y1 > ts.touchY[0] )
			 key_pressed = i;
	}
	
	if( 10> key_pressed)			// digit pressed.
	{
		if(KEY_BOARD_DIGITS>GKeyBoard.digits_length)
		{
			GKeyBoard.digits[GKeyBoard.digits_length] = key_pressed;
			GKeyBoard.digits_length++;
			GToDrawFLG = 1;												// Redraw display.
			return;
		}
	}
	
	if(BCKSP_POS == key_pressed)		// bcsp pressed.
	{
		if(0<GKeyBoard.digits_length)
			GKeyBoard.digits_length--;
		GToDrawFLG = 1;												// Redraw display.
		return;
	}
		
	// ESC if empty space pressed:
	if(GKeyBoard.Coord[0] < ts.touchX[0] && 
			 ts.touchX[0] < GKeyBoard.Coord[1] &&
		   GKeyBoard.Coord[2] < ts.touchY[0] &&
			 ts.touchY[0] < GKeyBoard.Coord[3] )
	{
	GKeyBoard.digits_length = 0;
	GKeyBoard.Active = 0;
	GToDrawFLG = 1;												// Redraw display.
	return;
	}
	
	if(ENTER_POS == key_pressed)
	{
		uint32_t temp = 0;
		uint32_t decimals = 1;
		
		for(uint8_t i=0; i<(GKeyBoard.digits_length-1); i++)
			decimals = decimals*10;
		for(uint8_t i=0; i<GKeyBoard.digits_length; i++)
		{
			temp += GKeyBoard.digits[i]*decimals;
		  decimals = decimals/10;
		}
			
		if(temp<=GKeyBoard.FMax && temp>=GKeyBoard.FMin)
		{
			*(GKeyBoard.Target) = temp;
			if(temp > 400000000)								// TI: for "Levi" only!
				GInjChanged = 1;									// TI: for "Levi" only!
			else																// TI: for "Levi" only!
				GParamChanged = 1;								// TI: for "Levi" only!
		}
		GKeyBoard.digits_length = 0;
		GKeyBoard.Active = 0;
		GToDrawFLG = 1;												// Redraw display.
		return;
	}
}


void KeyBoard_Draw(void)
{
	char str[10];
	
		// Rectangular background
	GUI_RECT RectBck = {180, 62, 467, 270};
	GUI_SetColor(GUI_BLUE_00);
	GUI_FillRectEx(&RectBck);
	RectBck.x0 = 10;
	RectBck.x1 = 180;
	RectBck.y0 = 180;
	RectBck.y1 = 250;
	GUI_FillRectEx(&RectBck);
	
	GUI_SetFont(GUI_FONT_24_1);
	
	for(uint8_t i=0; i<12; i++)	// draw keys
	{
		GUI_SetColor(GUI_LIGHTYELLOW);
		GUI_FillRectEx(&key[i]);
		GUI_SetColor(GUI_BLACK);
		GUI_DrawRectEx(&key[i]);
		GUI_SetTextMode(GUI_TM_TRANS);
		if(10>i)
		{
			sprintf(str, "%d", i);
		}
		else
		{
			switch(i)
			{
				case BCKSP_POS:
					sprintf(str, "Bcksp");
					break;
				case ENTER_POS:
					sprintf(str, "Enter");
					break;
				default:
					break;
			}
		}
		GUI_DispStringAt(str, (key[i].x0+key[i].x1)/2 - GUI_GetStringDistX(str)/2,  (key[i].y0+key[i].y1)/2 -GUI_GetFontSizeY()/2 );
		// TI: locate name of button in the rectangle centre
	}
	
	// draw frame
	GUI_RECT Rect = {GKeyBoard.Coord[0]+1, GKeyBoard.Coord[2]+1, GKeyBoard.Coord[1]-1, GKeyBoard.Coord[3]-1};
	GUI_SetColor(GUI_RED);
	GUI_DrawRoundedFrame(GKeyBoard.Coord[0] - 4, GKeyBoard.Coord[2] - 15, GKeyBoard.Coord[1] + 4, GKeyBoard.Coord[3] + 4, 4, 2);
	GUI_SetColor(GUI_WHITE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_BLACK);
	for(uint8_t i =0; i <GKeyBoard.digits_length; i++)
	{
		str[i] = GKeyBoard.digits[i] + '0';
	}
	str[GKeyBoard.digits_length] = '\0';
	GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
}




