#include "GraphicTask/GraphWarning.h"
#include "GraphicTask/MeasureWin.h"
#include "Flash/FlashMap.h"

TWarningWin		GWarningWin;

void WarningWin_Init(void)
{
	GWarningWin.Active 	= 0;
}

// If user have just clicked "Current" or "Time":
void WarningWin_Process(__IO TS_StateTypeDef  ts)
{	
	if( ts.touchX[0] > 335 &&  ts.touchX[0] < 455				// knob "Continue" is pushed
			&&  ts.touchY[0] > 205 && ts.touchY[0] < 245)
	{
		GKeyBoard.Active = 1;
		GWarningWin.Active = 0;
		GToDrawFLG = 1;												// Redraw display.
	}
	if( ts.touchX[0] > 205 &&  ts.touchX[0] < 325				// knob "Cancel" is pushed
			&&  ts.touchY[0] > 205 && ts.touchY[0] < 245)
	{
		GWarningWin.Active = 0;
		GToDrawFLG = 1;												// Redraw display.
	}
}

// If uSD card isn't inserted, for erasing the flash in case of reloading DataBase:
void WarningWin_Process2(__IO TS_StateTypeDef  ts)
{
	if( ts.touchX[0] > 335 &&  ts.touchX[0] < 455				// knob "Continue" is pushed
		&&  ts.touchY[0] > 205 && ts.touchY[0] < 245)
	{
		EraseFlash();
		NVIC_SystemReset();
	}
	if( ts.touchX[0] > 205 &&  ts.touchX[0] < 325				// knob "Cancel" is pushed
		&&  ts.touchY[0] > 205 && ts.touchY[0] < 245)
	{
		NVIC_SystemReset();
	}
}

void WarningWin_Process3(__IO TS_StateTypeDef  ts)
{
if( ts.touchX[0] > 335 &&  ts.touchX[0] < 455					// knob "Continue" is pushed
	&&  ts.touchY[0] > 205 && ts.touchY[0] < 245)
	{
		GWarningWin.Active = 0;
		GToDrawFLG = 1;												// Redraw display.
	}
	if( ts.touchX[0] > 205 &&  ts.touchX[0] < 325				// knob "Cancel" is pushed
		&&  ts.touchY[0] > 205 && ts.touchY[0] < 245)
	{
		GKeyBoard.Coord[0] = FIELDS_X0;
		GKeyBoard.Coord[1] = FIELDS_X1;
		GKeyBoard.Coord[2] = FIELD1_Y0;
		GKeyBoard.Coord[3] = FIELD1_Y1;
		GKeyBoard.FMin = 0x00FFFFFF;
		GKeyBoard.FMax = 0xFFFFFFFF;
		GKeyBoard.Target = &(GSelectedInj);
		GKeyBoard.Active = 1;
		GWarningWin.Active = 0;
		GToDrawFLG = 1;												// Redraw display.
	}
}
	

void WarningWin_Draw(void)
{		
	// Rectangular background
	GUI_RECT RectBck = {180, 62, 467, 270};
	GUI_SetColor(GUI_BLUE_00);
	GUI_FillRectEx(&RectBck);
	RectBck.x0 = 10;
	RectBck.x1 = 180;
	RectBck.y0 = 180;
	RectBck.y1 = 250;
	GUI_FillRectEx(&RectBck);
	
	for(uint8_t i = 2; i<2*SCREEN_SECTORS_HORIZONTAL_CNT; i++)
	{
		hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
														 GScreen_sector[i].FYBegin, 
														 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin-X_SECTORS_OVERLAP, 
														 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();

		// Rectangular window "Warning"
		GUI_RECT Rect = {195, 85, 465, 250};
		GUI_SetColor(GUI_BLUE_01);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_DrawRectEx(&Rect);
		GUI_SetTextMode(GUI_TM_TRANS);
		
		// Rectangular label of window "Error"
		Rect.x0 = 195;
		Rect.x1 = 465;
		Rect.y0 = 85;
		Rect.y1 = 115;
		GUI_SetColor(GUI_BLUE_02);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_DrawRectEx(&Rect);
		GUI_SetFont(GUI_FONT_20_1);
		Rect.x0 = 200;
		GUI_DispStringInRectWrap("WARNING", &Rect, GUI_TA_VCENTER |
																				GUI_TA_LEFT, GUI_WRAPMODE_WORD);
		
		// Text of error
		Rect.x0 = 205;
		Rect.x1 = 455;
		Rect.y0 = 120;
		Rect.y1 = 200;
		GUI_SetTextAlign(GUI_TA_CENTER);
		GUI_SetColor(GUI_BLACK);
		GUI_DispStringInRectWrap(GWarningWin.Text, &Rect, GUI_TA_VCENTER |
																				GUI_TA_CENTER, GUI_WRAPMODE_WORD);

															
		// button "Cancel"
		Rect.x0 = 205;
		Rect.x1 = 325;
		Rect.y0 = 205;
		Rect.y1 = 245;
		GUI_SetColor(GUI_BLUE_02);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_DrawRectEx(&Rect);
		GUI_DispStringInRectWrap("Cancel", &Rect, GUI_TA_VCENTER |
																				GUI_TA_CENTER, GUI_WRAPMODE_WORD);
																				
			// button "Continue"
		Rect.x0 = 335;
		Rect.x1 = 455;
		Rect.y0 = 205;
		Rect.y1 = 245;
		GUI_SetColor(GUI_BLUE_02);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLACK);
		GUI_DrawRectEx(&Rect);
		GUI_DispStringInRectWrap("Continue", &Rect, GUI_TA_VCENTER |
																				GUI_TA_CENTER, GUI_WRAPMODE_WORD);																		

		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}
