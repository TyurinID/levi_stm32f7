#include "GraphicTask/GraphError.h"
#include "uSD/uSD_API.h"

TErrorWin GErrorWin;

void ErrorWin_Init(void)
{
	GErrorWin.Active = 0;
	GErrorWin.Code = 0;
}

void ErrorWin_Draw(void)
{		
		for(uint8_t i = 0; i<2*SCREEN_SECTORS_HORIZONTAL_CNT; i++)
		{
			hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
															 GScreen_sector[i].FYBegin, 
															 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin-X_SECTORS_OVERLAP, 
															 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
			GUI_MEMDEV_Select(hMem);
			GUI_Clear();
	
			// Rectangular window "Error"
			GUI_RECT Rect = {140, 70, 340, 230};
			GUI_SetColor(GUI_BLUE_01);
			GUI_FillRectEx(&Rect);
			GUI_SetColor(GUI_BLACK);
			GUI_DrawRectEx(&Rect);
			
			// Rectangular label of window "Error"
			Rect.x0 = 140;
			Rect.x1 = 340;
			Rect.y0 = 70;
			Rect.y1 = 100;
			GUI_SetColor(GUI_BLUE_02);
			GUI_FillRectEx(&Rect);
			GUI_SetColor(GUI_BLACK);
			GUI_DrawRectEx(&Rect);
			GUI_SetFont(GUI_FONT_20_1);
			Rect.x0 = 150;
			GUI_DispStringInRectWrap("ERROR", &Rect, GUI_TA_VCENTER |
																					GUI_TA_LEFT, GUI_WRAPMODE_WORD);
			
			// Text of error
			Rect.x0 = 145;
			Rect.x1 = 335;
			Rect.y0 = 105;
			Rect.y1 = 180;
			GUI_SetTextAlign(GUI_TA_CENTER);
			GUI_SetColor(GUI_BLACK);
			GUI_DispStringInRectWrap(GErrorWin.Text, &Rect, GUI_TA_VCENTER |
																					GUI_TA_CENTER, GUI_WRAPMODE_WORD);


			// button "REPEAT"
			Rect.x0 = 190;
			Rect.x1 = 290;
			Rect.y0 = 185;
			Rect.y1 = 225;
			GUI_SetColor(GUI_BLUE_02);
			GUI_FillRectEx(&Rect);
			GUI_SetColor(GUI_BLACK);
			GUI_DrawRectEx(&Rect);
			GUI_DispStringInRectWrap("RESTART", &Rect, GUI_TA_VCENTER |
																					GUI_TA_CENTER, GUI_WRAPMODE_WORD);
																					
			GUI_MEMDEV_Select(0);
			GUI_MEMDEV_CopyToLCD(hMem);
			GUI_MEMDEV_Delete(hMem);
		}
}



void ErrorWin_Process(__IO TS_StateTypeDef ts)
{	
		NVIC_SystemReset();			// Restart system.
}

