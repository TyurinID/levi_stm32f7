

#include "GraphicTask/ScreenSectors.h"


TScreenSector GScreen_sector[2*SCREEN_SECTORS_HORIZONTAL_CNT];


void InitializeScreenSectors(void)
{
	GScreen_sector[0].FXBegin = 0;
	GScreen_sector[0].FXEnd   = 182+X_SECTORS_OVERLAP;
	GScreen_sector[0].FYBegin = 57;  // 205
	GScreen_sector[0].FYEnd 	= 113;
	
	GScreen_sector[1].FXBegin = 0;
	GScreen_sector[1].FXEnd   = 182+X_SECTORS_OVERLAP;
	GScreen_sector[1].FYBegin = 112;  // 205
	GScreen_sector[1].FYEnd 	= 270;

	GScreen_sector[2].FXBegin = 181;
	GScreen_sector[2].FXEnd   = 332+X_SECTORS_OVERLAP;
	GScreen_sector[2].FYBegin = 57;  // 205
	GScreen_sector[2].FYEnd 	= 113;	
	
	GScreen_sector[3].FXBegin = 181;
	GScreen_sector[3].FXEnd   = 332+X_SECTORS_OVERLAP;
	GScreen_sector[3].FYBegin = 112;  // 205
	GScreen_sector[3].FYEnd 	= 270;
	
	GScreen_sector[4].FXBegin = 331;
	GScreen_sector[4].FXEnd   = 479+X_SECTORS_OVERLAP;
	GScreen_sector[4].FYBegin = 57;  // 205
	GScreen_sector[4].FYEnd 	= 113;	
	
	GScreen_sector[5].FXBegin = 331;
	GScreen_sector[5].FXEnd   = 479+X_SECTORS_OVERLAP;
	GScreen_sector[5].FYBegin = 112;  // 205
	GScreen_sector[5].FYEnd 	= 270;
}


/*
void DrawLineInScreenSector(int x1, int y1, int x2, int y2, TScreenSector* screen_sector)
{
	// swap points
	
	//for(uint8_t i =0; i<3; i++)
	//{
		uint8_t do_draw = 0;
		if(x1 == x2)	// VERTICAL line
		{
			if( (x1>=screen_sector->FXBegin && x1 <=screen_sector->FXEnd) 
				|| (x2>=screen_sector->FXBegin && x2 <=screen_sector->FXEnd ) )
			{
				do_draw = 1;
			}
		}
		else
		{
			if(y1 == y2) // HORIZONTAL line
			{
				if( (y1>=screen_sector->FYBegin && y1 <=screen_sector->FYEnd)
				||  (y2>=screen_sector->FYBegin && y2 <=screen_sector->FYEnd) )
				{
					do_draw = 1;
				}
			}
			else
			{
				int x0, y0;			// diagonal line
				
				x0 = screen_sector->FXBegin;
				y0 = y1+(x0-x1)/(x2-x1)*(y2-y1);
				if( (y0>=y1 && y0<=y2) || (y0>=y2 && y0<=y1) )
				{
					do_draw = 1;
				}
				
				x0 = screen_sector->FXEnd;
				y0 = y1+(x0-x1)/(x2-x1)*(y2-y1);
				if( (y0>=y1 && y0<=y2) || (y0>=y2 && y0<=y1) )
				{
					do_draw = 1;
				}
				
				y0 = screen_sector->FYBegin;
				x0 = x1+(y0-y1)/(y2-y1)*(x2-x1);
				if( (x0>=x1 && x0<=x2) || (x0>=x2 && x0<=x1) )
				{
					do_draw = 1;
				}
				
				y0 = screen_sector->FYEnd;
				x0 = x1+(y0-y1)/(y2-y1)*(x2-x1);
				if( (x0>=x1 && x0<=x2) || (x0>=x2 && x0<=x1) )
				{
					do_draw = 1;
				}
				
			}
		}
		
		if(do_draw)
		{
			GUI_DrawLine(x1, y1, x2, y2);
		}
	//}
}


void DrawRectInScreenSector(int x1, int y1, int x2, int y2, TScreenSector* screen_sector)
{
	//int temp;
	// swap points
	*/
	/*if(x1>x2)
	{
		temp=x1;
		x1=x2;
		x2=temp;
	}		
	if(y1>y2)
	{
		temp=y1;
		y1=y2;
		y2=temp;
	}	*/	
	
	
	/*
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (x1 >= screen_sector->FXBegin && x1<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= x1 && screen_sector->FXBegin <= x2) ) &&
	    ( (y1 >= screen_sector->FYBegin && y1<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= y1 && screen_sector->FYBegin <= y2) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_RECT Rect = {x1, y1, x2, y2};
		GUI_DrawRectEx(&Rect);
	}
}

void FillRectInScreenSector(int x1, int y1, int x2, int y2, TScreenSector* screen_sector)
{
*/
	//int temp;
	// swap points
	/*if(x1>x2)
	{
		temp=x1;
		x1=x2;
		x2=temp;
	}		
	if(y1>y2)
	{
		temp=y1;
		y1=y2;
		y2=temp;
	}	*/	
	
	
	/*
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (x1 >= screen_sector->FXBegin && x1<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= x1 && screen_sector->FXBegin <= x2) ) &&
	    ( (y1 >= screen_sector->FYBegin && y1<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= y1 && screen_sector->FYBegin <= y2) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_RECT Rect = {x1, y1, x2, y2};
		GUI_FillRectEx(&Rect);
	}
}

void DrawEllipseInScreenSector(int x0, int y0, int rad_x, int rad_y, TScreenSector* screen_sector)
{
	int x1 = x0 - rad_x;
	int x2 = x0 + rad_x;
	int y1 = y0 - rad_x;
	int y2 = y0 + rad_x;
	
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (x1 >= screen_sector->FXBegin && x1<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= x1 && screen_sector->FXBegin <= x2) ) &&
	    ( (y1 >= screen_sector->FYBegin && y1<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= y1 && screen_sector->FYBegin <= y2) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_DrawEllipse(x0, y0, rad_x, rad_y);
	}
}

void FillEllipseInScreenSector(int x0, int y0, int rad_x, int rad_y, TScreenSector* screen_sector)
{
	int x1 = x0 - rad_x;
	int x2 = x0 + rad_x;
	int y1 = y0 - rad_x;
	int y2 = y0 + rad_x;
	
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (x1 >= screen_sector->FXBegin && x1<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= x1 && screen_sector->FXBegin <= x2) ) &&
	    ( (y1 >= screen_sector->FYBegin && y1<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= y1 && screen_sector->FYBegin <= y2) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_FillEllipse(x0, y0, rad_x, rad_y);
	}
}

void DrawTextInScreenSector(const char *s, int x1, int y1, TScreenSector* screen_sector)
{
	int x2 = x1+GUI_GetStringDistX(s);
	int y2 = y1+GUI_GetFontSizeY();
	
	// check do_draw
	uint8_t do_draw = 0;
	if( ( (x1 >= screen_sector->FXBegin && x1<=screen_sector->FXEnd) || 
		    (screen_sector->FXBegin >= x1 && screen_sector->FXBegin <= x2) ) &&
	    ( (y1 >= screen_sector->FYBegin && y1<=screen_sector->FYEnd) ||
				(screen_sector->FYBegin >= y1 && screen_sector->FYBegin <= y2) ) )
	{
		do_draw =1;
	}
	if(do_draw)
	{
		GUI_DispStringAt(s, x1, y1);
	}
}
*/

