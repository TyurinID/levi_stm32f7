/**
  ************************************************************************************************
  * @file	   LogPage.c
  * @author  Tyurin Ivan
  * @version V3.2
  * @date    19-December-2016
  * @brief   Display logs tables and react on screen touching.
  *************************************************************************************************
  */
	
#include "GraphicTask/LogPage.h"
#include "GraphicTask/GraphicTask.h"
#include "GraphicTask/ScreenSectors.h"
#include "GraphicTask/GraphicCommon.h"

tLog		Log; 
tLogWin		gLogWin;

static uint8_t logWinOutClickCnt;


void LogPage_Init(void)
{
	for(uint8_t i=0; i<LOG_LINES_BUFF_SIZE;i++)
		sprintf( Log.line[i] ,"");
	Log.currentLine = 0;
	logWinOutClickCnt=0;
	gLogWin.Active = 0;
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @LogPage_Process
  *	&		@brief  React on button pressing.
  * & 	@param  TouchParameter: ts.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void LogPage_Process(__IO TS_StateTypeDef  ts)
{
	// "Stardex" clicked ===========================:
	if(ts.touchX[0] > 5 &&  ts.touchX[0] < 160				
		&&  ts.touchY[0] > 5 && ts.touchY[0] < 60)
	{
		if(++logWinOutClickCnt >= 5)
		{
			logWinOutClickCnt = 0;
			gLogWin.Active = 0;
			GToDrawFLG = 1;						// Redraw graphics.
		}
	}
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @LogPage_Draw
  *	&		@brief  Draw logs tabels. 
	* & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void LogPage_Draw(void)
{			
	GUI_SetColor(GUI_BLUE_00);
	GUI_FillRect(476,2,478,4);	
	
	for(uint8_t i = 0; i<2*SCREEN_SECTORS_HORIZONTAL_CNT; i++)
	{
		hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
														 GScreen_sector[i].FYBegin, 
														 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin-X_SECTORS_OVERLAP, 
														 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();	
	
		GUI_RECT Rect;
		GUI_SetFont(&GUI_Font16_1);
		GUI_SetTextMode(GUI_TM_TRANS);
			
			
		// draw BIG fields:
		Rect.x0 = LOG_TABLE1_X0;
		Rect.x1 = LOG_TABLE1_X1;
		Rect.y0 = LOG_TABLE_Y0;
		Rect.y1 = LOG_TABLE_Y1;
		GUI_SetColor(GUI_WHITE);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLUE_02);
		GUI_DrawRectEx(&Rect);
		Rect.x0 = LOG_TABLE1_X0 + 3;
		for(uint8_t j=0; j<LOG_LINES_NUM/2; j++)
		{
			Rect.y0 = LOG_TABLE_Y0 + j*16;
			Rect.y1 = LOG_TABLE_Y0 + (j+1)*16;
			uint16_t line_cnt_in_buff = Log.currentLine + (LOG_LINES_BUFF_SIZE - LOG_LINES_NUM) + j + 1;
			if(line_cnt_in_buff >= LOG_LINES_BUFF_SIZE)
				line_cnt_in_buff -= LOG_LINES_BUFF_SIZE;
			GUI_DispStringInRectWrap(Log.line[line_cnt_in_buff], &Rect, GUI_TA_TOP |
																	GUI_TA_LEFT, GUI_WRAPMODE_WORD);
		}
		
		// draw BIG fields:
		Rect.x0 = LOG_TABLE2_X0;
		Rect.x1 = LOG_TABLE2_X1;
		Rect.y0 = LOG_TABLE_Y0;
		Rect.y1 = LOG_TABLE_Y1;
		GUI_SetColor(GUI_WHITE);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_BLUE_02);
		GUI_DrawRectEx(&Rect);
		Rect.x0 = LOG_TABLE2_X0 + 3;
		for(uint8_t j=0; j<LOG_LINES_NUM/2; j++)
		{
			Rect.y0 = LOG_TABLE_Y0 + j*16;
			Rect.y1 = LOG_TABLE_Y0 + (j+1)*16;
			uint16_t line_cnt_in_buff = Log.currentLine + (LOG_LINES_BUFF_SIZE - LOG_LINES_NUM) + j + (LOG_LINES_NUM/2) + 1;
			if(line_cnt_in_buff >= LOG_LINES_BUFF_SIZE)
				line_cnt_in_buff -= LOG_LINES_BUFF_SIZE;
			GUI_DispStringInRectWrap(Log.line[line_cnt_in_buff], &Rect, GUI_TA_TOP |
																	GUI_TA_LEFT, GUI_WRAPMODE_WORD);
		}
		
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @AddLog
  *	&		@brief  Add log string in log table for displaing on LogPage.
	* & 					Use following, for example: "254.2s  data=25.5".
	* & 	@param  *channel: pointer to channel to be logged;
	* &						*text: pointer to text of log.
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void AddLog(char* text)
{
	if(++Log.currentLine >= LOG_LINES_BUFF_SIZE)
		Log.currentLine = 0;
	strcpy(Log.line[Log.currentLine], text);		// Write new log string.
	GToDrawFLG = 1;						// Redraw logs menu.
}


