#include "GraphicTask/MeasureWin.h"
#include <stdlib.h>
#include "GraphicTask/LogPage.h"

TMeasureWin		GMeasureWin;
TInjector 		GSelectedInjector;						// Information about selected injector.

uint32_t	GSelectedInj;
uint8_t		GMeasurementStart;
uint8_t		GInjChanged;
uint8_t 	GParamChanged;
uint32_t 	GCurrent;											// Selected Current value.
uint32_t 	GTime;												// Selected Time value.

uint8_t		GMessageWin_Active;

static uint8_t logWinClickCnt;

static void DrawProgressBar(uint16_t delay);

const char* ImageType[9] =
{
	"CRI1",
	"CRI2.0",
	"CRI2.1",
	"CRI2.2",
	"CRIN1",
	"CRIN2-A",
	"CRIN3",
	"CRIN3-L",
	"",
};

void MeasureWin_Init(void)						// Create buttons and inscriptions first time. Initialize structure.
{			
	GMeasureWin.Active 	= 1;
	GMeasurementStart 	= 0;
	GCurrent = 3;
	GTime = 100;
	GInjChanged = 0;
	GParamChanged = 1;										// Send initial parameters to stm32f3 after discovery start.
	GSelectedInj = 0;
	GSelectedInjector.InjectorCode = 0;		// Injector is not selected.
	// todo: load from eeprom
	GSelectedInjector.ImageType = 0;

	for(uint8_t i=0; i<5; i++)
	{		
		GSelectedInjector.ParamNominal[i] = 0.0;
		GSelectedInjector.ParamTolerance[i] = 0.0;
	}	
	
	logWinClickCnt = 0;
}

void MeasureWin_Draw(void)						// Create buttons and inscriptions.
{	
	for(uint8_t i = 0; i<2*SCREEN_SECTORS_HORIZONTAL_CNT; i++)
	{
		hMem = GUI_MEMDEV_Create(GScreen_sector[i].FXBegin, 
															 GScreen_sector[i].FYBegin, 
															 GScreen_sector[i].FXEnd-GScreen_sector[i].FXBegin-X_SECTORS_OVERLAP, 
															 GScreen_sector[i].FYEnd-GScreen_sector[i].FYBegin);
		GUI_MEMDEV_Select(hMem);
		GUI_Clear();
	
		// Rectangular field "Injector Code"
		GUI_RECT Rect = {FIELDS_X0, FIELD1_Y0, FIELDS_X1 , FIELD1_Y1};
		GUI_SetColor(GUI_WHITE);
		GUI_FillRectEx(&Rect);
		GUI_SetColor(GUI_DARKBLUE);
		GUI_DrawRectEx(&Rect);
		Rect.y0 = LABEL1_Y0;
		Rect.y1 = LABEL1_Y1;
		Rect.x1 = (FIELDS_X1 + FIELDS_X0)/2;
		GUI_SetFont(GUI_FONT_16B_1);
		GUI_DispStringInRectWrap("Injector:", &Rect, GUI_TA_VCENTER |
																					GUI_TA_LEFT, GUI_WRAPMODE_WORD);
		// Table
		Rect.x0 = COLUMN1_X0;
		Rect.x1 = COLUMN3_X1;
		Rect.y0 = LINE1_Y0;
		Rect.y1 = LINE6_Y1;
		GUI_DrawRectEx(&Rect);
		Rect.y0 = LINE2_Y0;
		Rect.y1 = LINE3_Y0;
		GUI_DrawRectEx(&Rect);
		Rect.y0 = LINE4_Y0;
		Rect.y1 = LINE5_Y0;
		GUI_DrawRectEx(&Rect);
		Rect.y0 = LINE6_Y0;
		Rect.y1 = LINE6_Y1;
		GUI_DrawRectEx(&Rect);
		Rect.x0 = COLUMN2_X0;
		Rect.x1 = COLUMN3_X0;
		Rect.y0 = LINE1_Y0;
		GUI_DrawRectEx(&Rect);
		
		// Text in Table:
		Rect.x0 = COLUMN1_X0;
		Rect.x1 = COLUMN2_X0;
		Rect.y0 = LINE1_Y0;
		Rect.y1 = LINE2_Y0;
		GUI_SetFont(GUI_FONT_24_1);
		GUI_DispStringInRectWrap("Param", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		Rect.x0 = COLUMN2_X0;
		Rect.x1 = COLUMN3_X0;
		GUI_DispStringInRectWrap("Nominal", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);		
		Rect.x0 = COLUMN3_X0;
		Rect.x1 = COLUMN3_X1;
		GUI_DispStringInRectWrap("Tolerance", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);	
		Rect.x0 = COLUMN1_X0;
		Rect.x1 = COLUMN2_X0;
		Rect.y0 = LINE2_Y0;
		Rect.y1 = LINE3_Y0;
		GUI_DispStringInRectWrap("AH", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		Rect.y0 = LINE3_Y0;
		Rect.y1 = LINE4_Y0;
		GUI_DispStringInRectWrap("UEH", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);		
		Rect.y0 = LINE4_Y0;
		Rect.y1 = LINE5_Y0;
		GUI_DispStringInRectWrap("RLS", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		Rect.y0 = LINE5_Y0;
		Rect.y1 = LINE6_Y0;
		GUI_DispStringInRectWrap("AHE", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);		
		Rect.y0 = LINE6_Y0;
		Rect.y1 = LINE6_Y1;
		GUI_DispStringInRectWrap("DNH", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		
		if(GSelectedInjector.InjectorCode != 0)				// If injector is selected.
		{			

			// Text in field "Injector Code"
			char	str[INJECTOR_NAME_LENGTH] = "";
			Rect.x0 = FIELDS_X0;
			Rect.x1 = FIELDS_X1;
			Rect.y0 = FIELD1_Y0;
			Rect.y1 = FIELD1_Y1;
			GUI_SetColor(GUI_DARKBLUE);
			GUI_SetFont(GUI_FONT_24_1);	
			sprintf(str, "0%d", GSelectedInjector.InjectorCode);
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			
			// Injector Image:
			GUI_RECT Rect = {(FIELDS_X1 + FIELDS_X0)/2, LABEL1_Y0, FIELDS_X1, LABEL1_Y1};
			GUI_SetFont(GUI_FONT_16B_1);
			GUI_DispStringInRectWrap(ImageType[GSelectedInjector.ImageType], &Rect, GUI_TA_VCENTER |
																					GUI_TA_CENTER, GUI_WRAPMODE_WORD);
																						
			// Field "Current"
			Rect.x0 = FIELDS_X0;
			Rect.x1 = 85;
			Rect.y0 = LABEL2_Y0;
			Rect.y1 = LABEL2_Y1;
			GUI_SetFont(GUI_FONT_16B_1);	
			GUI_DispStringInRectWrap("Current, A:", &Rect, GUI_TA_VCENTER |
																					GUI_TA_LEFT, GUI_WRAPMODE_WORD);
			Rect.y0 = FIELD2_Y0;
			Rect.y1 = FIELD2_Y1;
			GUI_SetColor(GUI_WHITE);
			GUI_FillRectEx(&Rect);
			GUI_SetColor(GUI_DARKBLUE);
			GUI_DrawRectEx(&Rect);
			GUI_SetFont(GUI_FONT_24_1);	
			sprintf(str, "%d", GCurrent);
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
																						
			// Field "Time"
			Rect.x0 = 95;
			Rect.x1 = FIELDS_X1;
			Rect.y0 = LABEL2_Y0;
			Rect.y1 = LABEL2_Y1;
			GUI_SetFont(GUI_FONT_16B_1);	
			GUI_DispStringInRectWrap("Time, ms:", &Rect, GUI_TA_VCENTER |
																					GUI_TA_LEFT, GUI_WRAPMODE_WORD);
			Rect.y0 = FIELD2_Y0;
			Rect.y1 = FIELD2_Y1;
			GUI_SetColor(GUI_WHITE);
			GUI_FillRectEx(&Rect);
			GUI_SetColor(GUI_DARKBLUE);
			GUI_DrawRectEx(&Rect);
			GUI_SetFont(GUI_FONT_24_1);	
			sprintf(str, "%d", GTime);
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
																						
			// Knob "START":
			Rect.x0 = FIELDS_X0;
			Rect.x1 = FIELDS_X1;
			Rect.y0 = KNOB_Y0;
			Rect.y1 = KNOB_Y1;
			GUI_SetColor(GUI_RED);
			GUI_FillRectEx(&Rect);
			GUI_SetColor(GUI_DARKBLUE);
			GUI_DrawRectEx(&Rect);
			GUI_SetFont(GUI_FONT_32_1);	
			GUI_DispStringInRectWrap("START", &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);	
		
			// Text in Table:
			Rect.x0 = COLUMN2_X0;
			Rect.x1 = COLUMN3_X0;
			Rect.y0 = LINE2_Y0;
			Rect.y1 = LINE3_Y0;
			GUI_SetFont(GUI_FONT_24_1);		// "AH.nominal"
			if(GSelectedInjector.ParamNominal[0] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamNominal[0]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE3_Y0;
			Rect.y1 = LINE4_Y0;								// "UEH.nominal"
			if(GSelectedInjector.ParamNominal[1] != 0.0f)
				sprintf(str, "%.4f", GSelectedInjector.ParamNominal[1]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE4_Y0;
			Rect.y1 = LINE5_Y0;								// "UEH.nominal"
			if(GSelectedInjector.ParamNominal[2] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamNominal[2]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE5_Y0;
			Rect.y1 = LINE6_Y0;
			if(GSelectedInjector.ParamNominal[3] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamNominal[3]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE6_Y0;
			Rect.y1 = LINE6_Y1;								// "DNH.nominal"
			if(GSelectedInjector.ParamNominal[4] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamNominal[4]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			
			Rect.x0 = COLUMN3_X0;
			Rect.x1 = COLUMN3_X1;
			Rect.y0 = LINE2_Y0;
			Rect.y1 = LINE3_Y0;
			GUI_SetFont(GUI_FONT_24_1);		// "AH.Tolerance"
			if(GSelectedInjector.ParamTolerance[0] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamTolerance[0]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE3_Y0;
			Rect.y1 = LINE4_Y0;								// "UEH.Tolerance"
			if(GSelectedInjector.ParamTolerance[1] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamTolerance[1]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE4_Y0;
			Rect.y1 = LINE5_Y0;
			if(GSelectedInjector.ParamTolerance[2] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamTolerance[2]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE5_Y0;
			Rect.y1 = LINE6_Y0;
			if(GSelectedInjector.ParamTolerance[3] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamTolerance[3]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			Rect.y0 = LINE6_Y0;
			Rect.y1 = LINE6_Y1;								// "DNH.Tolerance"
			if(GSelectedInjector.ParamTolerance[4] != 0.0f)
				sprintf(str, "%g", GSelectedInjector.ParamTolerance[4]);
			else	sprintf(str, "---");
			GUI_DispStringInRectWrap(str, &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
			
			Rect.x0 = COLUMN1_X0;
			Rect.x1 = COLUMN3_X1;
			Rect.y0 = LINE6_Y1;
			Rect.y1 = SUBSCRIPTION_Y1;
			GUI_SetFont(GUI_FONT_16B_1);		
			GUI_DispStringInRectWrap("* all dimensions in mm", &Rect, GUI_TA_VCENTER |
																						GUI_TA_HCENTER, GUI_WRAPMODE_WORD);
		}
		
		GUI_MEMDEV_Select(0);
		GUI_MEMDEV_CopyToLCD(hMem);
		GUI_MEMDEV_Delete(hMem);
	}
}

void MeasureWin_Process(__IO TS_StateTypeDef  ts)
{
	//========== touch processing:
	// "Stardex" clicked ===========================:
	if(ts.touchX[0] > 5 &&  ts.touchX[0] < 160				
		&&  ts.touchY[0] > 5 && ts.touchY[0] < 60)
	{
		if(++logWinClickCnt >= 5)
		{
			logWinClickCnt = 0;
			gLogWin.Active = 1;
			GToDrawFLG = 1;						// Redraw graphics.
		}
	}
	
	if( ts.touchX[0] > FIELDS_X0 &&  ts.touchX[0] < FIELDS_X1				// label "injector Code" is pushed
	  &&  ts.touchY[0] > FIELD1_Y0 && ts.touchY[0] < FIELD1_Y1)
	{
		GKeyBoard.Coord[0] = FIELDS_X0;
		GKeyBoard.Coord[1] = FIELDS_X1;
		GKeyBoard.Coord[2] = FIELD1_Y0;
		GKeyBoard.Coord[3] = FIELD1_Y1;
		GKeyBoard.FMin = 0x00FFFFFF;
		GKeyBoard.FMax = 0xFFFFFFFF;
		GKeyBoard.Target = &(GSelectedInj);
		GKeyBoard.Active = 1;
		GToDrawFLG = 1;						// Draw KeyBoard.
	}			
	if(GSelectedInjector.InjectorCode != 0)				// If injector is selected.
	{
		if( ts.touchX[0] > FIELDS_X0 &&  ts.touchX[0] < 85				// label "Current" is pushed
				&&  ts.touchY[0] > FIELD2_Y0 && ts.touchY[0] < FIELD2_Y1)
		{
			GKeyBoard.Coord[0] = FIELDS_X0;
			GKeyBoard.Coord[1] = 85;
			GKeyBoard.Coord[2] = FIELD2_Y0;
			GKeyBoard.Coord[3] = FIELD2_Y1;
			GKeyBoard.FMin = 3;
			GKeyBoard.FMax = 25;
			GKeyBoard.Target = (&(GCurrent));	
			sprintf(GWarningWin.Text, "Changing the default values can result in damage to the injector!");		
			GWarningWin.Active  = 1;
			GToDrawFLG = 1;					// Draw WarrningWin.
		}
		
		if( ts.touchX[0] > 95 &&  ts.touchX[0] < FIELDS_X1				// label "Time" is pushed
				&&  ts.touchY[0] > FIELD2_Y0 && ts.touchY[0] < FIELD2_Y1)
		{
			GKeyBoard.Coord[0] = 95;
			GKeyBoard.Coord[1] = FIELDS_X1;
			GKeyBoard.Coord[2] = FIELD2_Y0;
			GKeyBoard.Coord[3] = FIELD2_Y1;
			GKeyBoard.FMin = 5;
			GKeyBoard.FMax = 3000;
			GKeyBoard.Target = (&(GTime));
			sprintf(GWarningWin.Text, "Changing the default values can result in damage to the injector!");				
			GWarningWin.Active = 1;
			GToDrawFLG = 1;					// Draw WarrningWin.
		}
			
		if( ts.touchX[0] > FIELDS_X0 &&  ts.touchX[0] < FIELDS_X1				// Knob "START" is pushed
				&&  ts.touchY[0] > KNOB_Y0 && ts.touchY[0] < KNOB_Y1)
		{
			GMeasurementStart = 1;
			osDelay(500);					// For UART.
			DrawWAIT();
			while(GMeasurementStart == 1);			
			GToDrawFLG = 1;					// Redraw display.	
		}
	}	
}	

void DrawWAIT(void)
{	
	if(GMeasurementStart == 0)
		return;
	
	hMem = GUI_MEMDEV_Create(FIELDS_X0-10, KNOB_Y0-10, FIELDS_X1+10, KNOB_Y1+10); 
	GUI_MEMDEV_Select(hMem);
	
	// Knob "WAIT":
	GUI_RECT Rect = {FIELDS_X0, KNOB_Y0, FIELDS_X1, KNOB_Y1};
	GUI_SetColor(GUI_LIGHTBLUE);
	GUI_FillRectEx(&Rect);
	GUI_SetColor(GUI_DARKBLUE);
	GUI_DrawRectEx(&Rect);
	GUI_SetFont(GUI_FONT_32_1);	
	GUI_DispStringInRectWrap("WAIT", &Rect, GUI_TA_VCENTER |
								GUI_TA_HCENTER, GUI_WRAPMODE_WORD);	
	GUI_MEMDEV_Select(0);
	GUI_MEMDEV_CopyToLCD(hMem);
	GUI_MEMDEV_Delete(hMem);
	
	uint16_t delay = (uint16_t)((9500 + GTime)/50);
	
	DrawProgressBar(delay);				//	TI:	delay		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

void DrawProgressBar(uint16_t delay)
{	
	for(uint8_t i = 0; i < 50; i++)
	{	
		if(GMeasurementStart == 0)
			return;
		
		// Progress bar "WAIT":
		GUI_RECT Rect = {FIELDS_X0+1, KNOB_Y0+1, FIELDS_X1, KNOB_Y1-1};		
		Rect.x1 = FIELDS_X0-1 + 3*(i+1);
		GUI_SetColor(GUI_BLUE_03);
		GUI_FillRectEx(&Rect);
		
		Rect.x0 = FIELDS_X0;
		Rect.x1 = FIELDS_X1;
		GUI_SetColor(GUI_DARKBLUE);
		GUI_SetFont(GUI_FONT_32_1);	
		GUI_DispStringInRectWrap("WAIT", &Rect, GUI_TA_VCENTER |
																					GUI_TA_HCENTER, GUI_WRAPMODE_WORD);	
		osDelay(delay);
	}
}
